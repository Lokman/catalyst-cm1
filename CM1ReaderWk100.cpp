/*
 * Based on Matthieu Dorier's source code 
 * AND ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#include "CM1ReaderWk100.hpp"

#define NPROC_768 0
#define X_SPACING 100
#define Y_SPACING 100 
#define Z_SPACING 200

#define NUM_PTS_GLOBAL_X 48000*2/X_SPACING
#define NUM_PTS_GLOBAL_Y 48000*2/Y_SPACING
#define NUM_PTS_GLOBAL_Z 20000/Z_SPACING

#define NUM_PTS_BLOCK_X 60
#define NUM_PTS_BLOCK_Y 60
#define NUM_PTS_BLOCK_Z 100


int CM1ReaderWk100::Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes)
{
//	int fvsa = 55;
//	LOG_CM1WK100_INFO(fvsa+" there!");
	// explore the dataset
	dataSet= dsetPath;
	list_iterations(dataSet.c_str());

	// initialize global vars
	dbzVar = uinterpVar = vinterpVar = NULL;
	xCoord = yCoord = zCoord = NULL;
	xDimSize = yDimSize = zDimSize = 0;
	currentIteration = 0;
		
	// get metadata: number of dimension, extents
	read_metadata(dsetPath);
	
	// intialize Grid and Attributes
	int mpiSize = 1;
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
	// TOFIX is using unsigned int really needed?
	unsigned int numLocalPoints[3] = {NUM_PTS_BLOCK_X, NUM_PTS_BLOCK_Y, NUM_PTS_BLOCK_Z};
	unsigned int numPoints[3] = {NUM_PTS_GLOBAL_X, NUM_PTS_GLOBAL_Y, NUM_PTS_GLOBAL_Z};
	// TOFIX that the wrong way to init spacing
	double spacing [3] = {X_SPACING, Y_SPACING, Z_SPACING};
		
	// TOFIX
	//grid.Initialize(numPoints, spacing, numLocalPoints);
	//grid.Initialize(numPoints, numLocalPoints);
	grid.Initialize(numPoints);
	attributes.Initialize(&grid);

	bid_tmp = grid.CreateBlock(numLocalPoints);

	grid.SetXLocalCoord(xCoord, bid_tmp);
	grid.SetYLocalCoord(yCoord, bid_tmp);
	grid.SetZLocalCoord(zCoord, bid_tmp);
		
	// TOFIX redundent with extents?
	grid.SetXDimSize(xDimSize, bid_tmp);
	grid.SetYDimSize(yDimSize, bid_tmp);
	grid.SetZDimSize(zDimSize, bid_tmp);
}

int CM1ReaderWk100::NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
	// TOFIX dont need the -1? do range check
	if (currentIteration == numIterations)
		return 1;
	
	int timeStep = iterations[currentIteration];
	do_iteration(mpiComm, timeStep);
	
	// initialize grid and attributes objects if the first call
//	if (currentIteration == 0) {
//		int mpiSize = 1;
//		MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
//		unsigned int numLocalPoints[3] = {xDimSize,yDimSize,zDimSize};
//		unsigned int numPoints[3] = {numLocalPoints[0]*mpiSize, numLocalPoints[1]*mpiSize, numLocalPoints[2]*mpiSize};
//		double spacing [3] = {X_SPACING, Y_SPACING, Z_SPACING};
//		
//		grid.Initialize(numPoints, spacing, numLocalPoints);
//		attributes.Initialize(&grid);
//
//		grid.SetXLocalCoord(xCoord, 0);
//		grid.SetYLocalCoord(yCoord, 0);
//		grid.SetZLocalCoord(zCoord, 0);
//		
//		grid.SetXDimSize(xDimSize, 0);
//		grid.SetYDimSize(yDimSize, 0);
//		grid.SetZDimSize(zDimSize, 0);
//	}
	
//	attributes.UpdateDbz(dbzVar, bid_tmp);
//	attributes.UpdateUinterp(uinterpVar, bid_tmp);
	attributes.UpdateUinterp(dbzVar, bid_tmp);
//	attributes.UpdateVinterp(vinterpVar, bid_tmp);
	attributes.SetTimeStep(timeStep);

	currentIteration++;
	return 0;
}

int CM1ReaderWk100::read_metadata(const std::string& dsetPath)
{
	int mpiRank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	char filename[256];
	const char var[]="dbz";
//	if ( ! NPROC_768)
		sprintf(filename,"%s/%s/%05d/%06d.h5",
			dsetPath.c_str(), var, iterations[0],mpiRank);
//	else
//		sprintf(filename,"%s/%s/%05d/%06d.h5",
//			dataSet.c_str(),dset,i,rank/3);
	int fvsa = 55;
	LOG_CM1WK100_INFO(" there!" << 55);

	if(mpiRank == 0)
		printf("Opening file for metadata %s\n",filename);
	hid_t fp = H5Fopen(filename,H5F_ACC_RDONLY,H5P_DEFAULT);
	if(fp < 0) return -1;

	int dim;
	H5LTget_dataset_ndims (fp,var,&dim);
	hsize_t* dims = (hsize_t*)malloc(sizeof(hsize_t)*dim);
	size_t type_size;
	H5T_class_t class_id;
	H5LTget_dataset_info (fp,var, dims, &class_id, &type_size);

	xDimSize = dims[2];
	yDimSize = dims[1];
	zDimSize = dims[0];

	// get block's coordinates
	xCoord = (float*) malloc(sizeof(float)*xDimSize);
	yCoord = (float*) malloc(sizeof(float)*yDimSize);
	zCoord = (float*) malloc(sizeof(float)*zDimSize);

	int offset_x = 0;
	int offset_y = 0;
	int offset_z = 0;

//	if (! NPROC_768 ){
		//int offset_x = (rank%16)*6000 - 48000 + pow(-1,rank)*10 - rank*10;
		offset_x = (mpiRank%16)*6000 - 48000;
		offset_y = (mpiRank/16)*6000 - 48000;
		//offset_y = (rank/16)*6000 - 48000 + pow(-1,rank)*10;
		offset_z = 0;
//	} else {
//		offset_x = ((rank/3)%16)*6000 - 48000;
//		offset_y = ((rank/3)/16)*6000 - 48000+(rank%3)*yDimSize*100;
//		offset_z = 0;
//	}

	int j;
	for(j=0; j<xDimSize; j++) xCoord[j] = (float)(offset_x + j*X_SPACING);
	for(j=0; j<yDimSize; j++) yCoord[j] = (float)(offset_y + j*Y_SPACING);
	for(j=0; j<zDimSize; j++) zCoord[j] = (float)(offset_z + j*Z_SPACING);

	
	if (! NPROC_768){
//		if(*ptr == NULL){
//			*ptr = (float*) malloc(type_size*xDimSize*yDimSize*zDimSize);	
//		}
//		H5LTread_dataset_float(fp,dset,*ptr);
		free(dims);
		H5Fclose(fp);
		return 0;
	}

//	// for 768 procs
//	int dims_768[3] = {0};
//	dims_768[0] = dims[0];
//	dims_768[1] = dims[1]/3;
//	dims_768[2] = dims[2];
//	
//	float* ptr_all = (float*) malloc(type_size*xDimSize*yDimSize*zDimSize);
//	H5LTread_dataset_float(fp,dset,ptr_all);
//
//	if(*ptr == NULL)
//		*ptr = (float*) malloc(type_size*xDimSize*yDimSize*zDimSize);
//
//	// get the data for this process
//	int j = 0;
//	char * all = (char*) ptr_all;
//	char * tmp = (char*) *ptr;
//	char * source = NULL;
//	int chunk_size = 0;
//	for ( j=0 ; j<(dims[0]*dims[1]*dims[2]) ; j=j+(dims[1]*dims[2]) ){
//		source = all+j*type_size+type_size*dims_768[1]*dims_768[2]*(rank%3);
//	        chunk_size = dims_768[1]*dims_768[2]*type_size;
//		memcpy( tmp, source, chunk_size);
//		tmp = tmp + dims_768[1]*dims_768[2]*type_size;
//	}
//	
//	xDimSize = dims_768[2];
//	yDimSize = dims_768[1];
//	zDimSize = dims_768[0];
//
//	free(dims);
//	free(ptr_all);
//
//	H5Fclose(fp);
	return 0;
}

int CM1ReaderWk100::read_datasets(const char* dset, int i, int rank, float** ptr)
{
	//int shared_rank = rank/3; // for 768 procs
	if(rank == 0)
		printf("Reading dataset %d\n",i);
	char filename[256];
	if ( ! NPROC_768)
		sprintf(filename,"%s/%s/%05d/%06d.h5",
			dataSet.c_str(),dset,i,rank);
	else
		sprintf(filename,"%s/%s/%05d/%06d.h5",
			dataSet.c_str(),dset,i,rank/3);

	if(rank == 0)
		printf("Opening file %s\n",filename);
	hid_t fp = H5Fopen(filename,H5F_ACC_RDONLY,H5P_DEFAULT);
	if(fp < 0) return -1;

	// TOFIX do we need to check number of dims and dims size each iteration?
	int dim;
	H5LTget_dataset_ndims (fp,dset,&dim);
	hsize_t* dims = (hsize_t*)malloc(sizeof(hsize_t)*dim);
	size_t type_size;
	H5T_class_t class_id;
	H5LTget_dataset_info (fp,dset, dims, &class_id, &type_size);

	xDimSize = dims[2];
	yDimSize = dims[1];
	zDimSize = dims[0];
	
	if (! NPROC_768){
		if(*ptr == NULL){
			*ptr = (float*) malloc(type_size*xDimSize*yDimSize*zDimSize);	
		}
		H5LTread_dataset_float(fp,dset,*ptr);
		free(dims);
		H5Fclose(fp);
		return 0;
	}

	// for 768 procs
	int dims_768[3] = {0};
	dims_768[0] = dims[0];
	dims_768[1] = dims[1]/3;
	dims_768[2] = dims[2];
	
	float* ptr_all = (float*) malloc(type_size*xDimSize*yDimSize*zDimSize);
	H5LTread_dataset_float(fp,dset,ptr_all);

	if(*ptr == NULL)
		*ptr = (float*) malloc(type_size*xDimSize*yDimSize*zDimSize);

	// get the data for this process
	int j = 0;
	char * all = (char*) ptr_all;
	char * tmp = (char*) *ptr;
	char * source = NULL;
	int chunk_size = 0;
	for ( j=0 ; j<(dims[0]*dims[1]*dims[2]) ; j=j+(dims[1]*dims[2]) ){
		source = all+j*type_size+type_size*dims_768[1]*dims_768[2]*(rank%3);
	        chunk_size = dims_768[1]*dims_768[2]*type_size;
		memcpy( tmp, source, chunk_size);
		tmp = tmp + dims_768[1]*dims_768[2]*type_size;
	}
	
	xDimSize = dims_768[2];
	yDimSize = dims_768[1];
	zDimSize = dims_768[0];

	free(dims);
	free(ptr_all);

	H5Fclose(fp);
	return 0;
}

int compare_ints(const void* a, const void* b)
{
	const int* ia = (const int*)a;
	const int* ib = (const int*)b;
	return (*ia > *ib) - (*ia < *ib);
}

void CM1ReaderWk100::list_iterations(const char* dirname)
{
	DIR* dp;
	struct dirent *ep;
	char* mydir = (char*) malloc(sizeof(char)*(5+strlen(dirname)));
	sprintf(mydir,"%s/dbz",dirname);
	dp = opendir(mydir);
	if(dp == NULL) {
		fprintf(stderr,"ERROR: cannot open directory %s.",mydir);
		MPI_Abort(MPI_COMM_WORLD,911);
	}
	
	numIterations = 0;
	while(ep = readdir(dp)) {
		if(strcmp(ep->d_name,".") != 0
		&& strcmp(ep->d_name,"..") != 0) {
			numIterations++;
		}
	}
	closedir(dp);

	iterations = (int*) malloc(sizeof(int)*numIterations);
	dp = opendir(mydir);
	int i = 0;
	while(ep = readdir(dp)) {
		if(strcmp(ep->d_name,".") != 0
		&& strcmp(ep->d_name,"..") != 0) {
			iterations[i] = atoi(ep->d_name);
			i++;
		}
	}
	closedir(dp);
	qsort (iterations,numIterations,sizeof(int),compare_ints);
	/*/ shortcut to play only the last 5 iterations
	iterations = &(iterations[8]);
	numIterations = 15;
	//*/
}

void CM1ReaderWk100::do_iteration(MPI_Comm comm, int i)
{
	int rank, err;
	MPI_Comm_rank(comm,&rank);
	err = read_datasets("dbz",i,rank,&dbzVar);
	if(err) MPI_Abort(MPI_COMM_WORLD,911);
	err = read_datasets("uinterp",i,rank,&uinterpVar);
	if(err) MPI_Abort(MPI_COMM_WORLD,911);
	err = read_datasets("vinterp",i,rank,&vinterpVar);
	if(err) MPI_Abort(MPI_COMM_WORLD,911);
	
	LOG_WORKING_ON_DEBUG("Data Sets dbz@" << dbzVar <<" uinterp@" << uinterpVar << " vinterp@" << vinterpVar << " loaded" );
	
	// do something with the datasets here
	if(xCoord != NULL) {
		return;
//		free(xCoord); free(yCoord); free(zCoord);
	}

//	xCoord = (float*) malloc(sizeof(float)*xDimSize);
//	yCoord = (float*) malloc(sizeof(float)*yDimSize);
//	zCoord = (float*) malloc(sizeof(float)*zDimSize);
//
//	int offset_x = 0;
//	int offset_y = 0;
//	int offset_z = 0;
//
//	if (! NPROC_768 ){
//		//int offset_x = (rank%16)*6000 - 48000 + pow(-1,rank)*10 - rank*10;
//		offset_x = (rank%16)*6000 - 48000;
//		offset_y = (rank/16)*6000 - 48000;
//		//offset_y = (rank/16)*6000 - 48000 + pow(-1,rank)*10;
//		offset_z = 0;
//	} else {
//		offset_x = ((rank/3)%16)*6000 - 48000;
//		offset_y = ((rank/3)/16)*6000 - 48000+(rank%3)*yDimSize*100;
//		offset_z = 0;
//	}
//
//	int j;
//	for(j=0; j<xDimSize; j++) xCoord[j] = (float)(offset_x + j*X_SPACING);
//	for(j=0; j<yDimSize; j++) yCoord[j] = (float)(offset_y + j*Y_SPACING);
//	for(j=0; j<zDimSize; j++) zCoord[j] = (float)(offset_z + j*Z_SPACING);

}

//} CM1Reader namespace

