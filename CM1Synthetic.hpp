/* 
 * This class generates synthetic CM1-like datasets. It is useful for debugging
 * There is mainly two ways to configure the grid and domain decomposition:
 *  1. The user should set the NUM_PTS_GLOBAL_x, NUM_PTS_BLOCK_x and ProcsNumberOver* variables 
 *  requirements => a. NUM_PTS_BLOCK_x must devide NUM_PTS_GLOBAL_x
 *    b. ProcsNumberOverx must devide 
 *        (NUM_PTS_GLOBAL_x / NUM_PTS_BLOCK_x)=The number of blocks over x dimension
 *    c. mpiSize must be a multiple of (ProcsNumberOverX*ProcsNumberOverY*ProcsNumberOverZ)
 *  2. The user should set the NUM_PTS_BLOCK_x, BlocksNumberOverDomainx and 
 *     ProcsNumberOver* variables 
 *  requirements => a.mpiSize must be a multiple of 
 *       (ProcsNumberOverX*ProcsNumberOverY*ProcsNumberOverZ)
 *  TOFIX: currently the class supports only the second configuration   
 */

#ifndef CM1SYNTHETIC_HPP
#define CM1SYNTHETIC_HPP

#include <string>
#include <mpi.h>
#include <math.h>

#include "CM1Producer.hpp"
#include "CM1DataStructures.hpp"

//#define NUM_PTS_GLOBAL_X 60
//#define NUM_PTS_GLOBAL_Y 60
//#define NUM_PTS_GLOBAL_Z 100
#define SYN_NUM_PTS_BLOCK_X 30//5//30
#define SYN_NUM_PTS_BLOCK_Y 30//30
#define SYN_NUM_PTS_BLOCK_Z 50//50

#define ProcsNumberOverX 2//4//10//2
#define ProcsNumberOverY 2//4//2
#define ProcsNumberOverZ 1//5//2
#define BlocksNumberOverDomainX 2
#define BlocksNumberOverDomainY 2
#define BlocksNumberOverDomainZ 1

#define dbg_BlocksNbr 
#define dbg_NUMITERS 1

//#define X_SPACING 100
//#define Y_SPACING 100 
//#define Z_SPACING 200

class CM1Synthetic : public CM1Producer
{
public:
	CM1Synthetic();
  int Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes);
	int NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
	// TOFIX lets keep these members public for know ...
	// TOFIX how to intergrate that with less effort in existing simulations
	// TOFIX when mem dynamically allocated
	//~CM1Synthetic() {delete dbz;}
private:
	int xDimProcsN;
	int yDimProcsN;
	int zDimProcsN;
	int wholeExtents[3]; 
	// TOFIX make it dynamic
	//float dbz[BlocksNumberOverDomainX*BlocksNumberOverDomainY*BlocksNumberOverDomainZ][SYN_NUM_PTS_BLOCK_X*SYN_NUM_PTS_BLOCK_Y*SYN_NUM_PTS_BLOCK_Z];

	int xDimBlocksN;
	int yDimBlocksN;
	int zDimBlocksN;
	// TOFIX solve the problem of int to unsigned int ONCE for ALL!!
	unsigned int blockExtents[3]; 
	int numBlocks;

	// TOFIX delete this var
  size_t bid_tmp;
	int currentIteration;
	int numIterations;
};

#endif

