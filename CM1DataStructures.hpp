/*
 * Based on ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#ifndef CM1_DATASTRUCTURES_H
#define CM1_DATASTRUCTURES_H

#include <cstddef>
#include <vector>
#include <unordered_map>

#include "mpi_mclog_m4.hpp"


class Block;
class Grid
{
public:
	Grid();
	void Initialize(const unsigned int numGlobalPoints[3]); // TOFIX  How about numGlobalCells
	unsigned int GetNumberOfAllPoints(); // TOFIX add multiblocks aupport
	unsigned int GetNumberOfAllCells();
	
	unsigned int GetNumberOfPoints(int blockId); 
	unsigned int GetNumberOfCells(int blockId);
	
	// TOFIX This has multiple ways of identifying a block:
	// block internal id, block index in the vector 
	// blocks indexs in the vector can change ...
	// TOFIX vtk functions dont take const as arguments
	float* GetXLocalCoord(int blockId);
	float* GetYLocalCoord(int blockId);
	float* GetZLocalCoord(int blockId);
	// TOFIX check if x/y/zDimSize are set?	
	int SetXLocalCoord(float* coord, int blockId);
	int SetYLocalCoord(float* coord, int blockId);
	int SetZLocalCoord(float* coord, int blockId);
	// TOFIX use GetBlock() within Grid's block related members
	int GetXDimSize(int blockId);
	int GetYDimSize(int blockId);
	int GetZDimSize(int blockId);
	
	int SetXDimSize(int dimSize, int blockId);
	int SetYDimSize(int dimSize, int blockId);
	int SetZDimSize(int dimSize, int blockId);
	
	// TOFIX are axis extents needed?
	// actually they are the same as DimSize vars,
	// so which ones are to be used DimSzie or Extents?
	unsigned int* GetExtents(int blockId);
//	unsigned int GetXExtent(int blockId);
//	unsigned int GetYExtent(int blockId);
//	unsigned int GetZExtent(int blockId);
	// TOFIX is it needed?
	int* GetGlobalExtents();

	// Blocks related members
	int GetNumberOfLocalBlocks();
	int GetNumberofAllBlocks();
	int CreateBlock();
	int CreateBlock(const unsigned int numLocalPoints[3]);
	// TOFIX const can be used to force the passage by Grid to 
	// execute non-const methods -> to change the Block's state
	//const Block* GetBlock(int blockId);
	Block* GetBlock(int blockId);
	// TOFIX may create confusion?
	Block* GetBlockAt(int blockId);


private:
	// TOFIX what are globall extents again?
	// They define the number of point on each axis
	int GlobalExtents[6];

	// An array is easier to iterate over - avoid using iterators
	std::vector<Block*> blocks;
	// To avoid getting the same hash for next block after deletion
	long long int blocksCounter;
	// More effective for lookup
	std::unordered_map<int, Block*> blockWithId;
};

class Attributes
{
public:
	Attributes();
	void Initialize(Grid* grid);

	int UpdateDbz(float* data, int blockId);
	int UpdateUinterp(float* data, int blockId);
	int UpdateVinterp(float* data, int blockId);
	void SetTimeStep(int timeStep);

	// vtk function dont take consts as arguments
	float* GetDbzArray(int blockId);
	float* GetUinterpArray(int blockId);
	float* GetVinterpArray(int blockId);

	int GetCurrentTimeStep();
private:
	int timeStep;
	Grid* GridPtr;
	
};

class Block
{
friend class Grid;
public:
	int GetId();
	
	// Grid related members
	unsigned int GetNumberOfPoints(); // TOFIX add multiblocks aupport
	unsigned int GetNumberOfCells();

	// vtk function dont take consts as arguments
	float* GetXCoord();
	float* GetYCoord();
	float* GetZCoord();
	
	void SetXCoord(float* coord);
	void SetYCoord(float* coord);
	void SetZCoord(float* coord);

	int GetXDimSize();
	int GetYDimSize();
	int GetZDimSize();
	// TOFIX redundent with extents?
	void SetXDimSize(int dimSize);
	void SetYDimSize(int dimSize);
	void SetZDimSize(int dimSize);
	
	unsigned int* GetExtents();
//	unsigned int GetXExtent();
//	unsigned int GetYExtent();
//	unsigned int GetZExtent();

	// Attributes related members
	void UpdateDbz(float* data);
	void UpdateUinterp(float* data);
	void UpdateVinterp(float* data);

	// vtk function dont take consts as arguments
	float* GetDbzArray();
	float* GetUinterpArray();
	float* GetVinterpArray();

	// TOFIX keep it for storing previous timesteps' data
	int GetCurrentTimeStep();


private:
	// TOFIX allow the construction only from Grid friend class?
	// TOFIX is it better to use Extent array instead of numLocalPoints array?
	Block();
	Block(const unsigned int numPoints[3]);
	// TOFIX use it as the main id
	int id;
	void SetId(int id);

	// Grid related members
	unsigned int Extents[6];
	float* xLocalCoord;
	float* yLocalCoord;
	float* zLocalCoord;

	// TOFIX redundent with extents
//	int xDimSize;
//	int yDimSize;
//	int zDimSize;

	// Attributes related members
	float* dbz;
	float* uinterp;
	float* vinterp;
	//Grid* GridPtr;

	int currentTimeStep;
};

#endif
