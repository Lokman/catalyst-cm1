/*
 * Based on ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <unistd.h>
#include <unordered_map>
#include <iostream>
#include <vector>
#include <string>

#include "CM1DataStructures.hpp"
#include "mpi_mclog_m4.hpp"

#define USE_CATALYST // TOFIX
#ifdef USE_CATALYST
#include "CM1Adaptor.hpp"
#endif

#ifdef PROD_WK100
#include "CM1ReaderWk100.hpp"
#endif

#ifdef PROD_BWC16DBZ
#include "CM1BWC16Dbz.hpp"
#endif

#ifdef PROD_SYNTH
#include "CM1Synthetic.hpp"
#endif

#ifdef USE_BIL
#include <bil.h>
#endif

//#define DO_FILTER // TOFIX
#ifdef DO_FILTER
#include "CM1Filter.hpp"
#endif

#ifdef DO_BALANCE
#include "CM1LoadBalance.hpp"
#endif

#ifdef DO_TIME_ADAPT
#include "CM1TimeAdaptor.hpp"
#endif

TIMER_INIT();

LOG_INIT();

int main(int argc, char** argv)
{
	int mpiRank, mpiSize;
	MPI_Init(&argc,&argv);
	MPI_Comm comm = MPI_COMM_WORLD;

  // Get logging command line options
  LOG_PARSE_OPTIONS(&argc,&argv); 
  
  // Configure Logging channels
  LOG_SET_COLOR_TIMER(LOG_GREEN_BKG);
  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
  
  // Timer
  LOG_SET_RUN_ID(comm);
  TIMER_SET("Program");

  // Mem usage
  unsigned long mainMemS= Utils::Mem::GetCurrentKB();
  unsigned long mainMemF = 0;
	
	MPI_Comm_rank(comm, &mpiRank);
	MPI_Comm_size(comm, &mpiSize);
	
#ifdef USE_BIL
	BIL_Init(MPI_COMM_WORLD);
#endif
  
  // TOFIX why only the process 0 can read commad line arguments
	if(mpiRank == 0) {
		if(argc < 2){
			fprintf(stderr,"Usage: %s dataset_path catalyst_python_script\n", argv[0]);
			MPI_Abort(MPI_COMM_WORLD, 911);
		}		
	}
	
	std::string pyCatalystScript("");
//	std::string dsetPath("."); // TOFIX is the dot needed?	
	std::string dsetPath(""); // TOFIX is the dot needed?	
	std::string coordFile("");
  // get the dataset path
	dsetPath += argv[1];
	// get the python script path
	// TODO update pyCatalyst according to defined preprocessor var
  if(argc >= 4) {
    // CM1BWC16 prod
    coordFile += argv[2];
    pyCatalystScript += argv[3];
  } else if(argc >= 3) {
    // Wk100 prod
    pyCatalystScript += argv[2];
  } else {
    // CM1Synthetic prod
    pyCatalystScript += argv[1];
  }

	// 
	Grid grid;
	Attributes attributes;
  
  CM1Producer* sim = nullptr;
// TODO change to #if defined to use else
#ifdef PROD_WK100
  sim = new CM1ReaderWk100();
#endif
#ifdef PROD_BWC16DBZ
  sim = new CM1BWC16Dbz(dsetPath, coordFile);
#endif
#ifdef PROD_SYNTH
  sim = new CM1Synthetic();
#endif
  
  TIMER_SET("ProdInit");
	sim->Initialize(dsetPath, grid, attributes);
  double res;
  TIMER_GET_MINE("ProdInit", &res, "init");
  
  LOG_TIMER_INFO(res 
    << " : CM1Producer::Initialize() : CM1Producer Initialize execution time");

#ifdef DO_FILTER
  float percentage = 0.1;
  Filter::Initialize(percentage);
#endif


#ifdef USE_CATALYST
  TIMER_SET("AdaptInit");
  Adaptor::Initialize(pyCatalystScript.c_str()); // TOFIX change the parameter to const string&
  TIMER_GET_MINE("AdaptInit", &res, "init");
  
  LOG_TIMER_INFO(res 
    << " : Adaptor::Initialize() : Catalyst Adaptor Initialize execution time");
  LOG_WORKING_ON_DEBUG("Catalyst Adaptor initialized with " 
      << pyCatalystScript.c_str() << " python script");
#endif

  // loop over iterations
  while (!sim->NextIteration(comm, grid, attributes)){
    MPI_Barrier(comm);
#ifdef DO_FILTER
    Filter::Filter(grid,attributes);
#endif
#ifdef USE_CATALYST
    // Timer
    TIMER_SET("AdaptCoProcess");
		Adaptor::CoProcess(grid, attributes);
    TIMER_GET_MINE("AdaptCoProcess", &res, attributes.GetCurrentTimeStep());

    LOG_TIMER_INFO(res 
      <<" : Adaptor::CoProcess() : Catalyst Adaptor CoProcess iteration " 
      << attributes.GetCurrentTimeStep() << " execution time");
#endif
	}

#ifdef USE_CATALYST
	Adaptor::Finalize();
#endif 

#ifdef DO_FILTER
  Filter::Finalize();
#endif

  // Timer
  TIMER_GET_MINE("Program", &res, "all");
  LOG_TIMER_INFO(res << " Main : All program execution time");
  
  // Mem
  mainMemF = Utils::Mem::GetCurrentKB();
  LOG_MEM_INFO("int main() : " << (mainMemF-mainMemS) 
    << " KB");
  LOG_WORKING_ON_INFO( "Run ID " << Utils::Log::getrunid(0));

#ifdef USE_BIL
	BIL_Finalize();
#endif
	MPI_Finalize();
	return 0;
}

