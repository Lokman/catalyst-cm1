#!/bin/bash

if [ "$CATLYST_PY_LD" = "YES" ]
then 
  echo "Env vars already exported."
else
  # ldconfig shared libraries for paraview/catalyst
  export LD_LIBRARY_PATH=/opt/lib/paraview-5.0/:$LD_LIBRARY_PATH
  export PYTHONPATH=$PYTHONPATH:/opt/lib/paraview-5.0/site-packages/
  export PYTHONPATH=$PYTHONPATH:/opt/lib/paraview-5.0/
  export PYTHONPATH=$PYTHONPATH::/opt/lib/paraview-5.0/site-packages/vtk
  export CATLYST_PY_LD=YES
  echo "Env vars exported."
fi

