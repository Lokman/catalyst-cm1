/*
 * Based on ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#ifndef CM1_ADAPTOR_CATALYST_HPP
#define CM1_ADAPTOR_CATALYST_HPP

#include <string>
#include "CM1DataStructures.hpp"

// TODO Change the file name to CM1Adaptor
namespace Adaptor
{

int Initialize(const char* scriptPath);
int CoProcess(Grid& grid, Attributes& attributes);
int Finalize();

}

#endif
