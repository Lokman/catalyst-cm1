library(ggplot2)

args <- commandArgs(TRUE)
df <- read.table(args[1], header=T, sep="\t")

# Get y-axis ticks corresponding to boxs values
#get_boxs_ticks <- function (data_frame) {
#  yticks <- c()
#  for (cb in  unique(df$codeblock)){
#    cbtime <- df$time[df$codeblock == cb]
#    q1 = quantile(cbtime)[2]
#    q3 = quantile(cbtime)[4]
#    iqr = q3 -q1
#    upper = q3+1.5*iqr
#    lower = q1-1.5*iqr
#
#    yticks <- c(yticks, q1, q3, iqr, upper, lower)
#  }
#  return (yticks)
#}

get_boxs_ticks <- function (box) {
  return (ggplot_build(box)$data[[1]]$middle)
}

## Geometric points and lines
#qplot(codeblock, time, data = df, geom = c("point", "line"), main = args[1], ylab = "time (s)")
g<- ggplot(df, aes(x=codeblock, y=time)) + labs(title = args[1])
#qplot(rank, time, data = df, geom = "point", main = args[1], ylab = "time (s)")

g <- g + geom_boxplot(fill="white", colour = "#3366FF", varwidth = TRUE, outlier.colour = "red", outlier.shape = 1)
#g<- g + scale_y_continuous(trans = "sqrt", breaks = get_boxs_ticks(df))
g <- g + scale_y_continuous(trans = "sqrt", breaks = c(get_boxs_ticks(g), min(df$time), max(df$time))) # TOFIX dont why ggplot is not showing small values ticks
#g<- g + theme( axis.text.y = element_text(size=20))

# Histogram
#qplot(rank, time, data = df, geom = "histogram", main = args[1], ylab= "time (s)")
#ggplot(df, aes(x=rank))+ geom_histogram(color="darkblue", fill="lightblue")
#ggplot(df) + geom_bar( aes(rank) )
#ggsave(paste(args[1],".png", sep=""), width=7, height=7, dpi=100)
#cat ("Plotting data file", args[1], "...\n")
ggsave(paste(args[1],"-box.png", sep=""))

## For min mean max plot
#library(plyr)
#
#cdf <- ddply(df, c('codeblock'), summarise, N = length(time), mean = mean(time), sd = sd(time), se = sd/sqrt(time), minval=min(time), maxval=max(time) )
#
#g <- ggplot(cdf, aes(x=codeblock, y=mean, colour=mean)) + labs(y=args[1])
#g <- g + geom_errorbar(aes(ymin=minval, ymax=maxval), width=.3) + geom_line() + geom_point()
#cat ("Plotting data file", args[1], "...\n")
#ggsave(paste(args[1],"-mmm.png", sep=""))

## Get min max and mean values
#library(reshape2)
#
#get_mmm_df <- function (data_frame) {
#  min_ <- c()
#  mea_ <- c()
#  max_ <- c()
#  cbs <- unique(df$codeblock)
#  for (cb in cbs){
#    min_ <- c(min_, min(df$time[df$codeblock == cb])  )
#    mea_ <- c(mea_, mean(df$time[df$codeblock == cb]))
#    max_ <- c(max_, max(df$time[df$codeblock == cb]))
#  }
#  ids <- seq(1, length(cbs))
#  mmm_df <- melt(data.frame(ids,cbs, min_,mea_,max_))
#  #colnames(mmm_df) <- c("codeblock", "minval","meanval","maxval")
#  return (mmm_df)
#}
#
#sdf <- get_mmm_df(df)
#g <- ggplot(sdf, aes(x=1, y=2, colour=2)) + labs(args[1])
#g <- g + geom_errorbar(aes(ymin=1, ymax=3), width=.3) + geom_line() + geom_point()
#
#ggsave(paste(args[1],"-mmm.png", sep=""))

