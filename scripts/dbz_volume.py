
try: paraview.simple
except: from paraview.simple import *

from paraview import coprocessing


#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      a1_uinterp_PiecewiseFunction = CreatePiecewiseFunction( Points=[-36.903831481933594, 0.0, 0.5, 0.0, 59.864414215087891, 1.0, 0.5, 0.0] )
      
      a3_cellNormals_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.99999996218892884, 0.0, 0.5, 0.0, 1.0000000295408549, 1.0, 0.5, 0.0] )
      
      a1_vtkCompositeIndex_PiecewiseFunction = CreatePiecewiseFunction( Points=[39.0, 0.0, 0.5, 0.0, 127.0, 1.0, 0.5, 0.0] )
      
      a3_Normals_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.9999999684917672, 0.0, 0.5, 0.0, 1.0000000288482147, 1.0, 0.5, 0.0] )
      
# AttributeError
#      a1_uinterp_PVLookupTable = GetLookupTableForArray( "uinterp", 1, RGBPoints=[-36.903831481933594, 0.23000000000000001, 0.29899999999999999, 0.754, 11.480291366577148, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 59.864414215087891, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a1_uinterp_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      a1_uinterp_PVLookupTable = GetLookupTableForArray( "uinterp", 1, RGBPoints=[-36.903831481933594, 0.23000000000000001, 0.29899999999999999, 0.754, 11.480291366577148, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 59.864414215087891, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      
# AttributeError
#      a3_cellNormals_PVLookupTable = GetLookupTableForArray( "cellNormals", 3, RGBPoints=[0.99999996218892884, 0.23000000000000001, 0.29899999999999999, 0.754, 0.99999999586489186, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 1.0000000295408549, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a3_cellNormals_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      a3_cellNormals_PVLookupTable = GetLookupTableForArray( "cellNormals", 3, RGBPoints=[0.99999996218892884, 0.23000000000000001, 0.29899999999999999, 0.754, 0.99999999586489186, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 1.0000000295408549, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      
# AttributeError
#      a1_vtkCompositeIndex_PVLookupTable = GetLookupTableForArray( "vtkCompositeIndex", 1, RGBPoints=[39.0, 0.23000000000000001, 0.29899999999999999, 0.754, 83.0, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 127.0, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a1_vtkCompositeIndex_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      a1_vtkCompositeIndex_PVLookupTable = GetLookupTableForArray( "vtkCompositeIndex", 1, RGBPoints=[39.0, 0.23000000000000001, 0.29899999999999999, 0.754, 83.0, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 127.0, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      
# AttributeError
#      a3_Normals_PVLookupTable = GetLookupTableForArray( "Normals", 3, RGBPoints=[0.9999999684917672, 0.23000000000000001, 0.29899999999999999, 0.754, 0.99999999866999101, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 1.0000000288482147, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ScalarOpacityFunction=a3_Normals_PiecewiseFunction, ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      a3_Normals_PVLookupTable = GetLookupTableForArray( "Normals", 3, RGBPoints=[0.9999999684917672, 0.23000000000000001, 0.29899999999999999, 0.754, 0.99999999866999101, 0.86499999999999999, 0.86499999999999999, 0.86499999999999999, 1.0000000288482147, 0.70599999999999996, 0.016, 0.14999999999999999], VectorMode='Magnitude', NanColor=[0.25, 0.0, 0.0], ColorSpace='Diverging', ScalarRangeInitialized=1.0 )
      
      RenderView1 = coprocessor.CreateView( CreateRenderView, "images/dBZ_volume_%t.png", 1, 0, 1, 892, 718 )
      RenderView1.CameraViewUp = [0.85826978609905891, 0.34735472973745424, 0.37777991741025074]
      RenderView1.CacheKey = 0.0
      RenderView1.StereoType = 0
      RenderView1.UseLight = 1
      RenderView1.StereoRender = 0
      RenderView1.CameraPosition = [143162.76125234127, -162110.81466673923, -175257.58243987529]
# AttributeError
#      RenderView1.StereoCapableWindow = 0
      RenderView1.OrientationAxesVisibility = 0
      RenderView1.Background2 = [0.0, 0.0, 0.16470588235294117]
      RenderView1.CameraClippingRange = [125342.58718628103, 467056.22263953928]
      RenderView1.Background = [1.0, 1.0, 1.0]
      RenderView1.LightIntensity = 0.48999999999999999
      RenderView1.CameraFocalPoint = [-9070.9923715570185, 12613.527343072201, 9946.2924566495349]
      RenderView1.CameraParallelScale = 45367.290108895439
      RenderView1.CenterOfRotation = [-9070.9923715570185, 12613.527343072201, 9946.2924566495349]
      
      ScalarBarWidgetRepresentation1 = CreateScalarBar( TitleFontSize=12, Title='uinterp', LookupTable=a1_uinterp_PVLookupTable, Visibility=0, LabelFontSize=12 )
      GetRenderView().Representations.append(ScalarBarWidgetRepresentation1)
      
      input_0_vtm = coprocessor.CreateProducer( datadescription, "input" )
      
      Contour1 = Contour( guiName="Contour1", Isosurfaces=[45.0], ContourBy=['POINTS', 'uinterp'], PointMergeMethod="Uniform Binning" )
      
      Plane2 = Plane( guiName="Plane2", Origin=[0.0, -59715.0, -59715.0], Point1=[0.0, 59715.0, -59715.0], Point2=[0.0, -59715.0, 59715.0] )
      
      SetActiveSource(input_0_vtm)
      DataRepresentation1 = Show()
      DataRepresentation1.Opacity = 0.67000000000000004
      DataRepresentation1.EdgeColor = [0.0, 0.0, 0.50000762951094835]
      DataRepresentation1.SelectionPointFieldDataArrayName = 'uinterp'
      DataRepresentation1.Visibility = 0
      DataRepresentation1.ScaleFactor = 11943.0
      
      SetActiveSource(Contour1)
      DataRepresentation4 = Show()
      DataRepresentation4.EdgeColor = [0.0, 0.0, 0.50000762951094835]
      DataRepresentation4.SelectionPointFieldDataArrayName = 'Normals'
      DataRepresentation4.DiffuseColor = [0.23920042725261312, 0.38469520103761351, 0.83027389944304575]
      DataRepresentation4.LookupTable = a3_Normals_PVLookupTable
      DataRepresentation4.ScaleFactor = 7577.9097656250005
      
      SetActiveSource(Plane2)
      DataRepresentation6 = Show()
      DataRepresentation6.ScaleFactor = 0.10000000000000001
      DataRepresentation6.EdgeColor = [0.0, 0.0, 0.50000762951094835]
      DataRepresentation6.SelectionPointFieldDataArrayName = 'Normals'
      DataRepresentation6.DiffuseColor = [0.72980849927519642, 0.77029068436713211, 0.68117799649042499]
      
    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  freqs = {'input': [1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(False)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
