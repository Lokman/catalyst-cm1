
from paraview.simple import *
from paraview import coprocessing


#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.
# ParaView 5.0.0 64 bits


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # state file generated using paraview version 5.0.0

      # ----------------------------------------------------------------
      # setup views used in the visualization
      # ----------------------------------------------------------------

      #### disable automatic camera reset on 'Show'
      paraview.simple._DisableFirstRenderCameraReset()

      # Create a new 'Render View'
      renderView1 = CreateView('RenderView')
      #renderView1.ViewSize = [848, 656]
      renderView1.CameraViewUp = [0.0, 0.0, 1.0]
      renderView1.CacheKey = 0.0
      renderView1.StereoType = 0
      renderView1.UseLight = 1
      renderView1.StereoRender = 0
      renderView1.CameraPosition = [-216627.96159490256, 47.1824617045786, -210.80174814110228]
      renderView1.LightSwitch = 0
      renderView1.OrientationAxesVisibility = 0
      renderView1.CameraClippingRange = [220242.82855121917, 225804.51614089642]
#      renderView1.StereoCapableWindow = 0
      renderView1.Background = [0.31999694819562063, 0.34000152590218968, 0.42999923704890519]
      renderView1.CameraFocalPoint = [5839.541015625, 47.1824617045786, -210.80174814110228]
      renderView1.CameraParallelScale = 118408.29658933092
      renderView1.CenterOfRotation = [5839.541015625, 47.1824617045786, -210.80174814110228]
#      renderView1.AxesGrid = 'GridAxes3DActor'
#      renderView1.CenterOfRotation = [-50.0, -50.0, 9210.787109375]
#      renderView1.StereoType = 0
##      renderView1.CameraPosition = [-50.0, -50.0, 273620.34106943867]
#      RenderView1.CameraPosition = [-216627.96159490256, 47.1824617045786, -210.80174814110228]
#      renderView1.CameraFocalPoint = [-50.0, -50.0, 9210.787109375]
#      renderView1.CameraParallelScale = 68434.22827192712
#      renderView1.Background = [0.32, 0.34, 0.43]

      # register the view with coprocessor
      # and provide it with information such as the filename to use,
      # how frequently to write the images, etc.
      coprocessor.RegisterView(renderView1,
          filename='imgs/bwc16_dbz_slice_%t.png', freq=1, fittoscreen=0, magnification=1, width=2200, height=2200, cinema={})
      renderView1.ViewTime = datadescription.GetTime()

      # ----------------------------------------------------------------
      # setup the data processing pipelines
      # ----------------------------------------------------------------

      # create a new 'XML MultiBlock Data Reader'
      # create a producer from a simulation input
      fullgrid_4800vtm = coprocessor.CreateProducer(datadescription, 'input')

      # create a new 'Slice'
      slice1 = Slice(Input=fullgrid_4800vtm)
      slice1.SliceType = 'Plane'
      slice1.SliceOffsetValues = [0.0]

      # init the 'Plane' selected for 'SliceType'
      slice1.SliceType.Origin = [5839.5422206740768, 0.0, 0.0]
#      slice1.SliceType.Normal = [1.0, 0.0, 0.0]

#      # create a new 'Parallel MultiBlockDataSet Writer'
#      parallelMultiBlockDataSetWriter1 = servermanager.writers.XMLMultiBlockDataWriter(Input=slice1)
#
#      # register the writer with coprocessor
#      # and provide it with information such as the filename to use,
#      # how frequently to write the data, etc.
#      coprocessor.RegisterWriter(parallelMultiBlockDataSetWriter1, filename='Dbz_slice_%t.vtm', freq=1)

#      # create a new 'Contour'
#      contour1 = Contour(Input=fullgrid_4800vtm)
#      contour1.ContourBy = ['POINTS', 'uinterp']
#      contour1.Isosurfaces = [-39.998, -12.203, 15.592000000000002, 43.387, 71.182]
#      contour1.PointMergeMethod = 'Uniform Binning'

      # ----------------------------------------------------------------
      # setup color maps and opacity mapes used in the visualization
      # note: the Get..() functions create a new object, if needed
      # ----------------------------------------------------------------

      # get color transfer function/color map for 'uinterp'
      uinterpLUT = GetColorTransferFunction('uinterp')
      uinterpLUT.RGBPoints = [-35.88689422607422, 0.231373, 0.298039, 0.752941, 13.786808013916016, 0.865003, 0.865003, 0.865003, 63.46051025390625, 0.705882, 0.0156863, 0.14902]
      uinterpLUT.ScalarRangeInitialized = 1.0

      # get opacity transfer function/opacity map for 'uinterp'
      uinterpPWF = GetOpacityTransferFunction('uinterp')
      uinterpPWF.Points = [-35.88689422607422, 0.0, 0.5, 0.0, 63.46051025390625, 1.0, 0.5, 0.0]
      uinterpPWF.ScalarRangeInitialized = 1

      # ----------------------------------------------------------------
      # setup the visualization in view 'renderView1'
      # ----------------------------------------------------------------

      # show data from slice1
      slice1Display = Show(slice1, renderView1)
      # trace defaults for the display properties.
      slice1Display.ColorArrayName = ['POINTS', 'uinterp']
      slice1Display.LookupTable = uinterpLUT
      slice1Display.GlyphType = 'Arrow'
      #slice1Display.SetScaleArray = ['POINTS', 'uinterp']
      #slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
      #slice1Display.OpacityArray = ['POINTS', 'uinterp']
      #slice1Display.OpacityTransferFunction = 'PiecewiseFunction'

      # show color legend
      slice1Display.SetScalarBarVisibility(renderView1, True)

      # setup the color legend parameters for each legend in this view

      # get color legend/bar for uinterpLUT in view renderView1
      uinterpLUTColorBar = GetScalarBar(uinterpLUT, renderView1)
      uinterpLUTColorBar.Title = 'uinterp'
      uinterpLUTColorBar.ComponentTitle = ''
    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  # these are the frequencies at which the coprocessor updates.
  freqs = {'input': [1, 1, 1, 1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(False, 1)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
