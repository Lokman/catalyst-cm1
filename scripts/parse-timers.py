import sys
import csv
import subprocess

# Check arguments
if len(sys.argv) != 2:
  print 'usage: {} <plot-type> <timers-file>'.format(sys.argv[0])
  sys.exit()

# Open file
print '[Timers-Parser] Opening timers file "{}" ...'.format(sys.argv[1])
handle = open(sys.argv[1])

# Read file in csv format
print '[Timers-Parser] Reading file "{}" in csv format ...'.format(sys.argv[1])
reader = csv.reader(handle, delimiter='\t')

# Parse timers file line by line
print '[Timers-Parser] Parsing lines in format col0="[TIMERS]" col1=rank col2=CodeBlock col3=time col4=timestep ...'

# Store all timer stats in a multilevl hashtable
# The stats contains one y-axis variable, i.e., execution time and  3 x-axis 
#   variables: block of code, its time step (iteration) and the rank 
#   of process running it
#
# This can generates 1-D, 1.5-D, 2-D and 3-D plots
#   1-D plots:
#     - (iter, time) (, cb, rank) => for fixed code block and rank
#     - (rank, time) (, cb, iter)
#     - (cb, time) (, rank, iter)
#   1.5-D plots:
#     - same as 1-D while adding boxplots/error bars as a summary of Snd variable
#   2-D plots:
#     - (iter, rank, time) (, cb)
#     - ... etc
#   3-D plots:
#     - (iter, rank, cb, time)

tstats = dict()

for raw in reader:
#  print '[Timers-Parser] [Debug] Parsing line {} '.format(raw)
  if raw[0] != '[TIMER]':
#    print '[Timers-Parser] [Debug] Ignoring current line '
    continue;
  rank = raw[1]
  codeblock = raw[2]
  time = raw[3]
  iteration = raw[4]

  if not (iteration in tstats):
    tstats[iteration] = dict()
  
  if not (rank in tstats[iteration]):
    tstats[iteration][rank] = dict()
  
  # if the timers file is correct this test is not needed
  #if not (blk in tstats[iteration][rank]):
  tstats[iteration][rank][codeblock] = time;


# Generates different plots using R
# time-1D.R
# time-2D.R

# Parse plot types
plots = dict()
plots_has_arg = dict()
plots_arg = dict()

argvi = '1d-time-rank'
iteration = '5364' 
cblk = 'CM1BWC16ReadBlks'  
if argvi == '1d-time-rank':
  filename = '1d-time-rank'+'-blk-'+cblk+'-iter-'+iteration+'.csv'
  plotdata = open (filename, 'w')
  print '[Timers-Parser] Saving 1d-time-rank stats to file {} ...'.format(filename)
  plotdata.write('rank\ttime\n')
  for rkey in tstats[iteration].keys():
    plotdata.write('{}\t{}\n'.format(rkey,tstats[iteration][rkey][cblk]));
  plotdata.close()
  print '[Timers-Parser] Plotting stats {} using Rscript ...'.format(filename)
  subprocess.check_call(['Rscript', 'time-1D.R', filename], shell=False)  

argvi = '2d-time-rank-blk'
iteration = '5364' 
if argvi == '2d-time-rank-blk':
  filename = '2d-time-rank-blk'+'-iter-'+iteration+'.csv'
  plotdata = open (filename, 'w')
  print '[Timers-Parser] Saving 2d-time-rank-blk stats to file {} ...'.format(filename)
  plotdata.write('rank\ttime\tcodeblock\n')
  for rkey in tstats[iteration].keys():
    for cbkey in tstats[iteration][rkey].keys():
      plotdata.write('{}\t{}\t{}\n'.format(rkey,tstats[iteration][rkey][cbkey], cbkey));
  plotdata.close()
  print '[Timers-Parser] Plotting stats {} using Rscript ...'.format(filename)
  subprocess.check_call(['Rscript', 'time-2D.R', filename], shell=False)  
  
argvi = '15d-time-blk-rank'
iteration = '5364' 
if argvi == '15d-time-blk-rank':
  filename = '15d-time-blk-rank'+'-iter-'+iteration+'.csv'
  plotdata = open (filename, 'w')
  print '[Timers-Parser] Saving 15d-time-blk-rank stats to file {} ...'.format(filename)
  plotdata.write('codeblock\ttime\trank\n')
  for rkey in tstats[iteration].keys():
    for cbkey in tstats[iteration][rkey].keys():
      plotdata.write('{}\t{}\t{}\n'.format(cbkey,tstats[iteration][rkey][cbkey], rkey));
  plotdata.close()
  print '[Timers-Parser] Plotting stats {} using Rscript ...'.format(filename)
  subprocess.check_call(['Rscript', 'time-15D.R', filename], shell=False)  

  
  

# Debug check if dict is filled coorectly
def debug_ (tstats):
  print '[Timers-Parser] [Debug] First level keys: '
  iterkey = -1
  for key in tstats.keys():
    print '{} '.format(key)
    iterkey = key
  rankkey = -1
  print ('[Timers-Parser] [Debug] '
    'Second level keys of last 1st-lvl key {} : '.format(iterkey))
  for key in tstats[iterkey].keys():
    print '{} '.format(key)
    rankkey = key
  cbkey = -1
  print ('[Timers-Parser] [Debug] Third level keys of last 2nd-lvl key {}'
    'of last 1st-lvl key {} : '.format(rankkey, iterkey))
  for key in tstats[iterkey][rankkey].keys():
    print '{} '.format(key)
    cbkey = key

  print ('[Timers-Parser] [Debug] value of 3rd-lvl key {} of 2nd-lvl key {}'
    'of 1st-lvl key {} : {}'.format(cbkey, rankkey, iterkey, 
    tstats[iterkey][rankkey][cbkey]))

  # Debug - restore the exact file content from dict
  #   Update: apparently it's a bit hard as keys are listed in alphabetic order (by default?)
  for iterkey in tstats:
    for rankkey in tstats[iterkey]:
      for cbkey in tstats[iterkey][rankkey]:
        print '[TIMER]\t{}\t{}\t{}\t{}'.format(rankkey, cbkey, 
          tstats[iterkey][rankkey][cbkey], iterkey)


  



  


