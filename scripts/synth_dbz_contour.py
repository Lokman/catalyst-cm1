
from paraview.simple import *
from paraview import coprocessing


#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.
# ParaView 5.0.0 64 bits


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # state file generated using paraview version 5.0.0

      # ----------------------------------------------------------------
      # setup views used in the visualization
      # ----------------------------------------------------------------

      #### disable automatic camera reset on 'Show'
      paraview.simple._DisableFirstRenderCameraReset()

      # Create a new 'Render View'
      renderView1 = CreateView('RenderView')
      #renderView1.ViewSize = [, 656]
      renderView1.AxesGrid = 'GridAxes3DActor'
      renderView1.CenterOfRotation = [14.5, 14.5, 29.5]
      renderView1.StereoType = 0
      renderView1.CameraPosition = [-65.67941794902214, 52.04523196695255, 136.41208918257013]
      renderView1.CameraFocalPoint = [14.499999999999984, 14.500000000000002, 29.499999999999986]
      renderView1.CameraViewUp = [0.005162856586278075, 0.9447027023909196, -0.3278874029406488]
      renderView1.CameraParallelScale = 35.92700933837939
      renderView1.Background = [0.32, 0.34, 0.43]

      # register the view with coprocessor
      # and provide it with information such as the filename to use,
      # how frequently to write the images, etc.
      coprocessor.RegisterView(renderView1,
          filename='imgs/synth_dbz_contour_%t.png', freq=1, fittoscreen=0, magnification=1, width=848, height=656, cinema={})
      renderView1.ViewTime = datadescription.GetTime()

      # ----------------------------------------------------------------
      # setup the data processing pipelines
      # ----------------------------------------------------------------

      # create a new 'XML MultiBlock Data Reader'
      # create a producer from a simulation input
      fullgrid_15vtm = coprocessor.CreateProducer(datadescription, 'input')

      # create a new 'Contour'
      contour1 = Contour(Input=fullgrid_15vtm)
      contour1.ContourBy = ['POINTS', 'uinterp']
      contour1.Isosurfaces = [4.02223, 11.351759999999999, 18.681289999999997, 26.01082, 33.340349999999994, 40.669880000000006, 47.99941, 55.32894, 62.658469999999994, 69.988]
      contour1.PointMergeMethod = 'Uniform Binning'

#      # create a new 'Parallel MultiBlockDataSet Writer'
#      parallelMultiBlockDataSetWriter1 = servermanager.writers.XMLMultiBlockDataWriter(Input=contour1)
#
#      # register the writer with coprocessor
#      # and provide it with information such as the filename to use,
#      # how frequently to write the data, etc.
#      coprocessor.RegisterWriter(parallelMultiBlockDataSetWriter1, filename='synth_dbz_%t.vtm', freq=1)

      # ----------------------------------------------------------------
      # setup color maps and opacity mapes used in the visualization
      # note: the Get..() functions create a new object, if needed
      # ----------------------------------------------------------------

      # get color transfer function/color map for 'uinterp'
      uinterpLUT = GetColorTransferFunction('uinterp')
      uinterpLUT.RGBPoints = [4.022233486175537, 0.231373, 0.298039, 0.752941, 37.00509715080261, 0.865003, 0.865003, 0.865003, 69.98796081542969, 0.705882, 0.0156863, 0.14902]
      uinterpLUT.ScalarRangeInitialized = 1.0

      # get opacity transfer function/opacity map for 'uinterp'
      uinterpPWF = GetOpacityTransferFunction('uinterp')
      uinterpPWF.Points = [4.022233486175537, 0.0, 0.5, 0.0, 69.98796081542969, 1.0, 0.5, 0.0]
      uinterpPWF.ScalarRangeInitialized = 1

      # ----------------------------------------------------------------
      # setup the visualization in view 'renderView1'
      # ----------------------------------------------------------------

      # show data from fullgrid_15vtm
      fullgrid_15vtmDisplay = Show(fullgrid_15vtm, renderView1)
      # trace defaults for the display properties.
      fullgrid_15vtmDisplay.Representation = 'Outline'
      fullgrid_15vtmDisplay.ColorArrayName = ['POINTS', 'uinterp']
      fullgrid_15vtmDisplay.LookupTable = uinterpLUT
      fullgrid_15vtmDisplay.GlyphType = 'Arrow'
      #fullgrid_15vtmDisplay.SetScaleArray = ['POINTS', 'uinterp']
      #fullgrid_15vtmDisplay.ScaleTransferFunction = 'PiecewiseFunction'
      #fullgrid_15vtmDisplay.OpacityArray = ['POINTS', 'uinterp']
      #fullgrid_15vtmDisplay.OpacityTransferFunction = 'PiecewiseFunction'

      # show color legend
      fullgrid_15vtmDisplay.SetScalarBarVisibility(renderView1, True)

      # show data from contour1
      contour1Display = Show(contour1, renderView1)
      # trace defaults for the display properties.
      contour1Display.ColorArrayName = ['POINTS', '']
      contour1Display.GlyphType = 'Arrow'
      #contour1Display.SetScaleArray = [None, '']
      #contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
      #contour1Display.OpacityArray = [None, '']
      #contour1Display.OpacityTransferFunction = 'PiecewiseFunction'

      # setup the color legend parameters for each legend in this view

      # get color legend/bar for uinterpLUT in view renderView1
      uinterpLUTColorBar = GetScalarBar(uinterpLUT, renderView1)
      uinterpLUTColorBar.Title = 'uinterp'
      uinterpLUTColorBar.ComponentTitle = ''
    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  # these are the frequencies at which the coprocessor updates.
  freqs = {'input': [1, 1, 1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(False, 1)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
