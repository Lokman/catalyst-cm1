
from paraview.simple import *
from paraview import coprocessing


#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.
# ParaView 5.0.0 64 bits


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # state file generated using paraview version 5.0.0

      # ----------------------------------------------------------------
      # setup views used in the visualization
      # ----------------------------------------------------------------

      #### disable automatic camera reset on 'Show'
      paraview.simple._DisableFirstRenderCameraReset()

      # Create a new 'Render View'
      renderView1 = CreateView('RenderView')
      #renderView1.ViewSize = [6000, 6000]
      renderView1.AxesGrid = 'GridAxes3DActor'
      renderView1.CenterOfRotation = [14.499999523162842, 14.499999523162842, 29.500000715255737]
      renderView1.StereoType = 0
      renderView1.CameraPosition = [14.499999523162842, 14.499999523162842, 196.15015747237013]
      renderView1.CameraFocalPoint = [14.499999523162842, 14.499999523162842, 29.500000715255737]
      renderView1.CameraParallelScale = 43.132234438061744
      renderView1.Background = [0.32, 0.34, 0.43]

      # register the view with coprocessor
      # and provide it with information such as the filename to use,
      # how frequently to write the images, etc.
      coprocessor.RegisterView(renderView1,
          filename='imgs/synth_dbz_slice_%t.png', freq=1, fittoscreen=0, magnification=1, width=1000, height=1000, cinema={})
      renderView1.ViewTime = datadescription.GetTime()

      # ----------------------------------------------------------------
      # setup the data processing pipelines
      # ----------------------------------------------------------------

      # create a new 'XML MultiBlock Data Reader'
      # create a producer from a simulation input
      fullgrid_18vtm = coprocessor.CreateProducer(datadescription, 'input')

      # create a new 'Slice'
      slice1 = Slice(Input=fullgrid_18vtm)
      slice1.SliceType = 'Plane'
      slice1.SliceOffsetValues = [0.0]

      # init the 'Plane' selected for 'SliceType'
      slice1.SliceType.Origin = [18.637477205430688, 13.470223545388308, 33.02721760369618]
      slice1.SliceType.Normal = [0.0, 0.0, 1.0]

      # ----------------------------------------------------------------
      # setup color maps and opacity mapes used in the visualization
      # note: the Get..() functions create a new object, if needed
      # ----------------------------------------------------------------

      # get color transfer function/color map for 'uinterp'
      uinterpLUT = GetColorTransferFunction('uinterp')
      uinterpLUT.RGBPoints = [4.665655136108398, 0.231373, 0.298039, 0.752941, 37.28945446014404, 0.865003, 0.865003, 0.865003, 69.91325378417969, 0.705882, 0.0156863, 0.14902]
      uinterpLUT.ScalarRangeInitialized = 1.0

      # get opacity transfer function/opacity map for 'uinterp'
      uinterpPWF = GetOpacityTransferFunction('uinterp')
      uinterpPWF.Points = [4.665655136108398, 0.0, 0.5, 0.0, 69.91325378417969, 1.0, 0.5, 0.0]
      uinterpPWF.ScalarRangeInitialized = 1

      # ----------------------------------------------------------------
      # setup the visualization in view 'renderView1'
      # ----------------------------------------------------------------

      # show data from slice1
      slice1Display = Show(slice1, renderView1)
      # trace defaults for the display properties.
      slice1Display.ColorArrayName = ['POINTS', 'uinterp']
      slice1Display.LookupTable = uinterpLUT
      slice1Display.GlyphType = 'Arrow'
      #slice1Display.SetScaleArray = [None, 'uinterp']
      #slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
      #slice1Display.OpacityArray = [None, 'uinterp']
      #slice1Display.OpacityTransferFunction = 'PiecewiseFunction'

      # show color legend
      slice1Display.SetScalarBarVisibility(renderView1, True)

      # setup the color legend parameters for each legend in this view

      # get color legend/bar for uinterpLUT in view renderView1
      uinterpLUTColorBar = GetScalarBar(uinterpLUT, renderView1)
      uinterpLUTColorBar.Title = 'uinterp'
      uinterpLUTColorBar.ComponentTitle = ''
    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  # these are the frequencies at which the coprocessor updates.
  freqs = {'input': [1, 1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(False, 1)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
