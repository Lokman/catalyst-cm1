
from paraview.simple import *
from paraview import coprocessing


#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.
# ParaView 5.0.0 64 bits


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      # state file generated using paraview version 5.0.0

      # ----------------------------------------------------------------
      # setup views used in the visualization
      # ----------------------------------------------------------------

      #### disable automatic camera reset on 'Show'
      paraview.simple._DisableFirstRenderCameraReset()

      # Create a new 'Render View'
      renderView1 = CreateView('RenderView')
#      renderView1.ViewSize = [848, 656]
#      renderView1.AxesGrid = 'GridAxes3DActor'
#      renderView1.CenterOfRotation = [3296.0703125, 15188.75, 9210.787109375]
#      renderView1.StereoType = 0
#      renderView1.CameraPosition = [-36553.18486325891, -189281.25409973884, 68741.64974182865]
#      renderView1.CameraFocalPoint = [3296.0703124999886, 15188.750000000005, 9210.787109375]
#      renderView1.CameraViewUp = [0.007299981251647679, 0.2782210520561723, 0.9604893317817135]
#      renderView1.CameraParallelScale = 56074.727099685224
#      renderView1.Background = [0.32, 0.34, 0.43]
      renderView1.CameraViewUp = [0.85826978609905891, 0.34735472973745424, 0.37777991741025074]
      renderView1.CacheKey = 0.0
      renderView1.StereoType = 0
      renderView1.UseLight = 1
      renderView1.StereoRender = 0
      renderView1.CameraPosition = [143162.76125234127, -162110.81466673923, -175257.58243987529]
      renderView1.OrientationAxesVisibility = 0
      renderView1.Background2 = [0.0, 0.0, 0.16470588235294117]
      renderView1.CameraClippingRange = [125342.58718628103, 467056.22263953928]
      renderView1.Background = [1.0, 1.0, 1.0]
      renderView1.LightIntensity = 0.48999999999999999
      renderView1.CameraFocalPoint = [-9070.9923715570185, 12613.527343072201, 9946.2924566495349]
      renderView1.CameraParallelScale = 45367.290108895439
      renderView1.CenterOfRotation = [-9070.9923715570185, 12613.527343072201, 9946.2924566495349]

      # register the view with coprocessor
      # and provide it with information such as the filename to use,
      # how frequently to write the images, etc.
      coprocessor.RegisterView(renderView1,
          filename='imgs/bwc16_dbz_contour_%t.png', freq=1, fittoscreen=0, magnification=1, width=848, height=656, cinema={})
      renderView1.ViewTime = datadescription.GetTime()

      # ----------------------------------------------------------------
      # setup the data processing pipelines
      # ----------------------------------------------------------------

      # create a new 'XML MultiBlock Data Reader'
      # create a producer from a simulation input
      fullgrid_4800vtm = coprocessor.CreateProducer(datadescription, 'input')

      # create a new 'Slice'
      slice1 = Slice(Input=fullgrid_4800vtm)
      slice1.SliceType = 'Plane'
      slice1.SliceOffsetValues = [0.0]

      # init the 'Plane' selected for 'SliceType'
      slice1.SliceType.Origin = [24885.0, 24885.0, 7065.0]
      slice1.SliceType.Normal = [0.0, 0.0, 1.0]

      # create a new 'Contour'
      contour1 = Contour(Input=fullgrid_4800vtm)
      contour1.ContourBy = ['POINTS', 'uinterp']
      contour1.Isosurfaces = [-36, -13.2, 9.6, 32.4, 70]
      contour1.PointMergeMethod = 'Uniform Binning'

#      # create a new 'Parallel MultiBlockDataSet Writer'
#      parallelMultiBlockDataSetWriter1 = servermanager.writers.XMLMultiBlockDataWriter(Input=contour1)
#
#      # register the writer with coprocessor
#      # and provide it with information such as the filename to use,
#      # how frequently to write the data, etc.
#      coprocessor.RegisterWriter(parallelMultiBlockDataSetWriter1, filename='Dbz_contour_%t.vtm', freq=1)

      # ----------------------------------------------------------------
      # setup the visualization in view 'renderView1'
      # ----------------------------------------------------------------

      # show data from contour1
      contour1Display = Show(contour1, renderView1)
      # trace defaults for the display properties.
      contour1Display.ColorArrayName = [None, '']
      contour1Display.GlyphType = 'Arrow'
      #contour1Display.SetScaleArray = [None, '']
      #contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
      #contour1Display.OpacityArray = [None, '']
      #contour1Display.OpacityTransferFunction = 'PiecewiseFunction'

    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  # these are the frequencies at which the coprocessor updates.
  freqs = {'input': [1, 1, 1, 1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(False, 1)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription);

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22222)
