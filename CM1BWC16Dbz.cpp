/*
 * Partially based on Matthieu Dorier's source code 
*/

#include "CM1BWC16Dbz.hpp"

CM1BWC16Dbz::CM1BWC16Dbz(const std::string& dsetPath, const std::string& coordFile)
{
//  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
//  LOG_SET_COLOR_TIMER(LOG_GREEN_BKG);

//  LOG_SET_LEVEL_WORKING_ON(LOG_NONE);

  this->dsetPath = dsetPath;
  this->coordFile = coordFile;
 
  this->numIterations = 0;
  this->currentIteration = 0;

  LOG_CM1BWC16_DEBUG( "New CM1 reader created with dset in " 
      << this->dsetPath << " and coordinates file "
      << this->coordFile);
}

int CM1BWC16Dbz::Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes)
{
  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	
  int sqrtSize = sqrt(mpiSize);
	int nbrBlocksAll = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)*
          (NUM_PTS_GLOBAL_Y/NUM_PTS_BLOCK_Y)*
          (NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z);
  if ((mpiSize != sqrtSize*sqrtSize) && (nbrBlocksAll%mpiSize != 0)) {
		LOG_CM1BWC16_ERROR("The number of processes (" << mpiSize 
			<< ") must be a perfect square and must divide " << nbrBlocksAll);
		MPI_Abort(MPI_COMM_WORLD, 911);
	}

  LOG_CM1BWC16_INFO("Initializing CM1BWC16Dbz reader ... ");

  unsigned int numPoints[3];

  numPoints[0] = (unsigned int) NUM_PTS_GLOBAL_X;
	numPoints[1] = (unsigned int) NUM_PTS_GLOBAL_Y;
	numPoints[2] = (unsigned int) NUM_PTS_GLOBAL_Z;

  // Initialize the Grid and Attributes
	grid.Initialize(numPoints);
	attributes.Initialize(&grid);
  
  // Get iterations
  list_iterations();

  // Get coordinates
  read_global_coordinates();

//  TOFIX
//  int procNbrPerXYDim = sqrt(mpiSize);
//  int blockNbrPerXYDimProc = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/procNbrPerXYDim;
  int n = sqrt(mpiSize);
  int c = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/n; // blocks in this process along the x or y dimension

//  TOFIX
//  //int procExtents[6];
//  //int xProcStartExtent = mpiRank%ProcNbrPerXYDim;
//  int nnx = mpiRank%procNbrPerXYDim;
//  int nny = mpiRank/procNbrPerXYDim;
//  int blockNbrPerZDim = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;
  int nnx = mpiRank % n;
  int nny = mpiRank / n;
  int d = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

  int first_id = (nny*n*c+nnx)*c*d;
  size_t ttl_size = 0;
  int blk = 0;
  this->blocksNbr = 0;
  //TOFIX is it needed? should it start from non-zero values?
  unsigned int blockExtents[3] = {NUM_PTS_BLOCK_X, NUM_PTS_BLOCK_Y, NUM_PTS_BLOCK_Z};

//  // Timer
//  double bwc16LocalS = MPI_Wtime();
//  double bwc16LocalF = 0;
  TIMER_SET("CM1BWC16InitBlks");
//  int id = mpiRank*250;
  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
//	      int bx = NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X;
//	      int by = NUM_PTS_GLOBAL_Y/NUM_PTS_BLOCK_Y;
//	      int bz = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

	      int start_z = id % d;
	      int start_x = (id/d) % (c*n);
	      int start_y = (id/d) / (c*n);

	      start_x *= NUM_PTS_BLOCK_X;
	      start_z *= NUM_PTS_BLOCK_Z;
	      start_y *= NUM_PTS_BLOCK_Y;

	      int end_x = start_x + NUM_PTS_BLOCK_X - 1;
	      int end_y = start_y + NUM_PTS_BLOCK_Y - 1;
	      int end_z = start_z + NUM_PTS_BLOCK_Z - 1;
	
        // compute ghost zones
	      if(start_x != 0)  { start_x -= 1; }
	      if(start_y != 0)  { start_y -= 1; }
	      if(start_z != 0)  { start_z -= 1; }
        
        int bid_tmp = grid.CreateBlock();
		    this->blocksNbr++; // TOFIX is it needed
        float* xCoordBlock = new float[end_x-start_x+1];// TOFIX Check failure
		    float* yCoordBlock = new float[end_y-start_y+1];// TOFIX Check failure
		    float* zCoordBlock = new float[end_z-start_z+1];// TOFIX Check failure
		
        std::memcpy(xCoordBlock, &(xCoordGlobal[0])+start_x, 
            (size_t) (end_x-start_x+1) * sizeof(float));
        std::memcpy(yCoordBlock, &(yCoordGlobal[0])+start_y, 
            (size_t) (end_y-start_y+1) * sizeof(float));
        std::memcpy(zCoordBlock, &(zCoordGlobal[0])+start_z, 
            (size_t) (end_z-start_z+1) * sizeof(float));
        
        grid.SetXLocalCoord(zCoordBlock, bid_tmp);
		    grid.SetYLocalCoord(yCoordBlock, bid_tmp);
		    grid.SetZLocalCoord(xCoordBlock, bid_tmp);
		
		    // TOFIX redundent with extents?
		    grid.SetXDimSize((end_z-start_z+1), bid_tmp);
		    grid.SetYDimSize((end_y-start_y+1), bid_tmp);
		    grid.SetZDimSize((end_x-start_x+1), bid_tmp);

        LOG_CM1BWC16_DEBUG("New block " << blk << "(" << bid_tmp 
              <<" - simId " << id  << ") created with coordinates "
              <<"x[" << xCoordBlock[0] << " ... " 
              << xCoordBlock[end_x-start_x] << "] - "
              <<"y[" << yCoordBlock[0] << " ... "
              << yCoordBlock[end_y-start_y] << "] - "
              <<"z[" << zCoordBlock[0] << " ... "
              << zCoordBlock[end_z-start_z] << "]");
        
        id += 1;
        blk += 1;

      }
    }
    first_id += c*n*d;
  }
  
//  // Timer
//  bwc16LocalF = MPI_Wtime();
  double res;
  TIMER_GET_MINE("CM1BWC16InitBlks", &res, "init");
  LOG_TIMER_INFO(res << " : BWC16DbzSLBC : Setup local blocks coordinates");

  LOG_CM1BWC16_INFO("CM1BWC16Dbz Initialized: " 
        << " Whole Grid initialized and " << grid.GetNumberOfLocalBlocks() 
        << " blocks have been created");

  return 0;
}

int CM1BWC16Dbz::NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
  // debug
	if (currentIteration == 5)
    return 1;
  
  int ret = -1;

#ifdef USE_MPI_IO
  ret = NextIterationMPIIO(mpiComm, grid, attributes);
#endif

#ifdef USE_BIL
  ret = NextIterationBIL(mpiComm, grid, attributes);
#endif

#ifdef USE_CEPH_FILE
  ret = NextIterationCephFile(mpiComm, grid, attributes);
#endif

#ifdef USE_CEPH_OBJECTS
  ret = NextIterationCephObjects(mpiComm, grid, attributes);
#endif

#ifdef USE_CEPH_STORE
  ret = NextIterationCephStore(mpiComm, grid, attributes);
#endif

  return ret;
}

// Use MPI I/O to read iteration data files
#ifdef USE_MPI_IO
int CM1BWC16Dbz::NextIterationMPIIO(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
//  TIMER_SET("CM1BWC16IterMPIIO");

  // TOFIX dont need the -1? do range check
	if (currentIteration == numIterations)
    return 1;

  LOG_CM1BWC16_INFO("CM1BWC16Dbz MPIIO loading data for iteration " << currentIteration
        << "(" << this->iters[currentIteration]  << ") ...");

  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

//  TOFIX
//  int procNbrPerXYDim = sqrt(mpiSize);
//  int blockNbrPerXYDimProc = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/procNbrPerXYDim;
  int n = sqrt(mpiSize);
  int c = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/n; // blocks in this process along the x or y dimension

//  TOFIX
//  //int procExtents[6];
//  //int xProcStartExtent = mpiRank%ProcNbrPerXYDim;
//  int nnx = mpiRank%procNbrPerXYDim;
//  int nny = mpiRank/procNbrPerXYDim;
//  int blockNbrPerZDim = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;
  int nnx = mpiRank % n;
  int nny = mpiRank / n;
  int d = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

  std::string dataFilePath = this->dsetPath + 
                        this->iters[this->currentIteration];
	MPI_File fh;
  LOG_CM1BWC16_DEBUG("Opening file " << dataFilePath.c_str() << " ...");
  MPI_File_open(MPI_COMM_SELF, dataFilePath.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  // TOFIX test for open failure 

  int first_id = (nny*n*c+nnx)*c*d;
  size_t ttl_size = 0;
  int blk = 0;
  
  TIMER_SET("CM1BWC16ReadBlksMPIIO");

//  int id = mpiRank*250;
  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
        int bid_tmp = grid.GetBlockAt(blk)->GetId();
        
        LOG_CM1BWC16_INFO("Updating block " << blk << "(" << bid_tmp 
          << " - simId " << id << ") ...");
        
	      int start_z = id % d;
	      int start_x = (id/d) % (c*n);
	      int start_y = (id/d) / (c*n);

	      start_z *= NUM_PTS_BLOCK_Z;
	      start_x *= NUM_PTS_BLOCK_X;
	      start_y *= NUM_PTS_BLOCK_Y;

	      int end_x = start_x + NUM_PTS_BLOCK_X - 1;
	      int end_y = start_y + NUM_PTS_BLOCK_Y - 1;
	      int end_z = start_z + NUM_PTS_BLOCK_Z - 1;
        
        // compute ghost zones
	      if(start_x != 0)  { start_x -= 1; }
	      if(start_y != 0)  { start_y -= 1; }
	      if(start_z != 0)  { start_z -= 1; }

        int size_big[3];
        size_big[0] = 2200;//NUM_PTS_GLOBAL_X;
        size_big[1] = 2200;//NUM_PTS_GLOBAL_Y;
        size_big[2] = 380;//NUM_PTS_GLOBAL_Z;
	
        int size_small[3];
        size_small[0] = end_x - start_x + 1;
        size_small[1] = end_y - start_y + 1;
        size_small[2] = end_z - start_z + 1;

        int offsets[3];
        offsets[0] = start_x;
        offsets[1] = start_y;
        offsets[2] = start_z;
        
        // TOFIX HUGE memory leak!!! Need to free previous arrays
        float* dbz = new float[(end_x-start_x+1)*(end_y-start_y+1)*(end_z-start_z+1)];	

	      MPI_Datatype cube;
        MPI_Type_create_subarray(3,size_big,size_small,
                offsets,MPI_ORDER_C,MPI_FLOAT,&cube);

        // TOFIX from function
        //void block::read(MPI_File fh)

        unsigned int count = (end_x-start_x+1)*(end_y-start_y+1)*(end_z-start_z+1);

        MPI_Type_commit(&cube);

        MPI_File_set_view(fh, 0, MPI_FLOAT, cube, "native", MPI_INFO_NULL);
        MPI_Status status;
        int err = MPI_File_read(fh, dbz, count, MPI_FLOAT, &status);
        
        MPI_Type_free(&cube);
        
//        // Read blocks data
//        MPI_Offset offset = start_y*(n*c*d)*count + start_x*(d)*NUM_PTS_BLOCK_Z*NUM_PTS_BLOCK_X +
//          start_z*NUM_PTS_BLOCK_Z;
//        float* offset_dbz = dbz;
            
        //MPI_File_set_view(fh, 0, MPI_FLOAT, MPI_FLOAT, "native", MPI_INFO_NULL);
//        for (int a=0 ; a<NUM_PTS_BLOCK_Y ; a++) {
//          for (int b=0 ; b<NUM_PTS_BLOCK_X; b++){
//            MPI_File_seek(fh, offset, MPI_SEEK_SET);
//            MPI_Status status;
//            int err = MPI_File_read(fh, offset_dbz, NUM_PTS_BLOCK_Z, MPI_FLOAT, &status);
//            offset += d*NUM_PTS_BLOCK_Z;
//            offset_dbz += NUM_PTS_BLOCK_Z;
//          }
//          offset += (n*c*d)*NUM_PTS_BLOCK_X*NUM_PTS_BLOCK_Z;
//        }
//

        // Save the data to corresponding blocks

        // end of function
        attributes.UpdateDbz(dbz, bid_tmp);
        attributes.UpdateUinterp(dbz, bid_tmp);
        attributes.UpdateVinterp(dbz, bid_tmp);
        attributes.SetTimeStep(std::stoi(this->iters[this->currentIteration]));


        // Log data reading information
        LOG_CM1BWC16_DEBUG("Data @" << dbz << "-" 
          << attributes.GetDbzArray(bid_tmp) 
          << "of Block " << blk << " (" << bid_tmp 
          << " - simId " << id << ") Iteration " << attributes.GetCurrentTimeStep() 
          << " made available by CM1BWC16Dbz MPIIO");
        id += 1;
        blk+= 1;
      }
    }
    first_id += c*n*d;
  }

  MPI_File_close(&fh);

  currentIteration++;

  double res;
  TIMER_GET_MINE("CM1BWC16ReadBlksMPIIO", &res, attributes.GetCurrentTimeStep())
  LOG_TIMER_INFO(res << " : CM1BWC16Dbz::NextIterationMPIIO() : TimeStep " 
    << attributes.GetCurrentTimeStep());
  return 0;
}
#endif

//TOFIX: Does this scale?
int CM1BWC16Dbz::read_global_coordinates()
{
	// Open coordinates file
  std::ifstream ifs(this->coordFile.c_str());
	std::string line;
	int i = 0;
  // Read the file line by line
  // There is only three lines one line for each dimension coordinates
  
  if(!ifs){
    LOG_CM1BWC16_ERROR("Couldn't open coordinates file " << this->coordFile.c_str());
    MPI_Abort(MPI_COMM_WORLD, 911);
  }

  while (std::getline(ifs, line))
  {
    float c;
    std::istringstream iss(line);
    while(iss >> c) {
      if(i == 0) this->xCoordGlobal.push_back(c);
      if(i == 1) this->yCoordGlobal.push_back(c);
      if(i == 2) this->zCoordGlobal.push_back(c);
    }
    i += 1;
  }
  
  LOG_CM1BWC16_INFO("Global coordinates [" << xCoordGlobal.size() << ", " 
        << yCoordGlobal.size() << ", " << zCoordGlobal.size() << "]" 
        << " read from file " << coordFile);

  LOG_CM1BWC16_DEBUG("xCoordGlobal [" << xCoordGlobal.front() << " ... "
        << xCoordGlobal.back() << "] - "
        << "yCoordGlobal [" << yCoordGlobal.front() << " ... "
        << yCoordGlobal.back() <<"] - "
        << "zCoordGlobal [" << zCoordGlobal.front() << " ... "
        << zCoordGlobal.back() <<"]");

  return 0;
}

//void block::set_coord_from_global()
//{
//	this->xCoordLocal = std::vector<float>(this->xCoordGlobal.begin()+start_x, 
//                        this->xCoordGlobal.begin()+(end_x+1));
//	this->yCoordLocal = std::vector<float>(this->yCoordGlobal.begin()+start_y,
//                        this->yCoordGlobal.begin()+(end_y+1));
//	this->zCoordLocal = std::vector<float>(this->zCoordGlobal.begin()+start_z, 
//                        this->zCoordGlobal.begin()+(end_z+1));
//}

int CM1BWC16Dbz::read_dataset()
{
  //TOFIX no need for that as list_iterations() has been already called in Init
//  std::string dirname(this->dsetPath);
//  std::vector<std::string> files = list_iterations(dirname);
//  std::sort(files.begin(),files.end());
  
  return 0;
}

int CM1BWC16Dbz::list_iterations()
{
#ifdef USE_CEPH_OBJECTS
  this->numIterations = 1;
  this->iters.push_back(std::string("05364"));
  this->iters.push_back(std::string("05374"));
  this->iters.push_back(std::string("05384"));
  this->iters.push_back(std::string("05394"));
  this->iters.push_back(std::string("05404"));
#else   
  DIR* dp;
  struct dirent *ep;
  dp = opendir(dsetPath.c_str());
  if(dp == NULL) {
    LOG_CM1BWC16_ERROR(" cannot open directory " << dsetPath);
    MPI_Abort(MPI_COMM_WORLD,911);
  }
  
  while(ep = readdir(dp)) {
    if(strcmp(ep->d_name,".") != 0 && strcmp(ep->d_name,"..") != 0) {
      //TOFIX define result as an attributes
      this->iters.push_back(std::string(ep->d_name));
    }
  }
  closedir(dp);
  std::sort(iters.begin(),iters.end());
  
  this->numIterations = iters.size();
  
  LOG_CM1BWC16_INFO(numIterations << " iterations listed in folder "
      << dsetPath);
#endif
  return 0;
}

// Use Block Io Library to readt iteration data files
#ifdef USE_BIL
int CM1BWC16Dbz::NextIterationBIL(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
  TIMER_SET("CM1BWC16IterBIL");

  // TOFIX dont need the -1? do range check
	if (currentIteration == numIterations)
    return 1;
  
  LOG_CM1BWC16_INFO("CM1BWC16Dbz BIL loading data for iteration " << currentIteration
        << "(" << this->iters[currentIteration]  << ") ...");

  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

  int n = sqrt(mpiSize);
  int c = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/n; // blocks in this process along the x or y dimension
  int nnx = mpiRank % n;
  int nny = mpiRank / n;
  int d = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

  std::string dataFilePath = this->dsetPath + 
                        this->iters[this->currentIteration];
  std::vector<float*> dbzs;

  int first_id = (nny*n*c+nnx)*c*d;
  size_t ttl_size = 0;
  int blk = 0;
  
  TIMER_SET("CM1BWC16ReadBlksBIL");

  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
        int bid_tmp = grid.GetBlockAt(blk)->GetId();
        
        LOG_CM1BWC16_INFO("Updating block " << blk << "(" << bid_tmp 
          << " - simId " << id << ") ...");
		
	      int start_z = id % d;
	      int start_x = (id/d) % (c*n);
	      int start_y = (id/d) / (c*n);

	      start_z *= NUM_PTS_BLOCK_Z;
	      start_x *= NUM_PTS_BLOCK_X;
	      start_y *= NUM_PTS_BLOCK_Y;

	      int end_x = start_x + NUM_PTS_BLOCK_X - 1;
	      int end_y = start_y + NUM_PTS_BLOCK_Y - 1;
	      int end_z = start_z + NUM_PTS_BLOCK_Z - 1;
        
        // compute ghost zones
	      if(start_x != 0)  { start_x -= 1; }
	      if(start_y != 0)  { start_y -= 1; }
	      if(start_z != 0)  { start_z -= 1; }

        int size_big[3];
        size_big[0] = 2200;//NUM_PTS_GLOBAL_X;
        size_big[1] = 2200;//NUM_PTS_GLOBAL_Y;
        size_big[2] = 380;//NUM_PTS_GLOBAL_Z;
	
        int size_small[3];
        size_small[0] = end_x - start_x + 1;
        size_small[1] = end_y - start_y + 1;
        size_small[2] = end_z - start_z + 1;

        int offsets[3];
        offsets[0] = start_x;
        offsets[1] = start_y;
        offsets[2] = start_z;
        
        // TOFIX HUGE memory leak!!! Need to free previous arrays
        float* dbz = new float[(end_x-start_x+1)*(end_y-start_y+1)*(end_z-start_z+1)];	


	      float* buffer[1];
        buffer[0] = dbz;
        BIL_Add_block_raw(3, size_big, offsets, size_small,
                       dataFilePath.c_str(), MPI_FLOAT, (void**) &buffer);
        dbzs.push_back(dbz);

        id += 1;
        blk+= 1;
      }
    }
    first_id += c*n*d;
  }

  LOG_CM1BWC16_DEBUG("Reading all blocks at once from file "
    << dataFilePath.c_str() << " using BIL...");
  
  BIL_Read();
  
  first_id = (nny*n*c+nnx)*c*d;
  ttl_size = 0;
  blk = 0;
  
  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
        int bid_tmp = grid.GetBlockAt(blk)->GetId();

        attributes.UpdateDbz(dbzs[blk], bid_tmp);
        attributes.UpdateUinterp(dbzs[blk], bid_tmp);
        attributes.UpdateVinterp(dbzs[blk], bid_tmp);
        attributes.SetTimeStep(std::stoi(this->iters[this->currentIteration]));
        
        LOG_CM1BWC16_DEBUG("Data @" << dbzs[blk] << "-" 
          << attributes.GetDbzArray(bid_tmp) 
          << "of Block " << blk << " (" << bid_tmp 
          << " - simId " << id << ") Iteration " << attributes.GetCurrentTimeStep() 
          << " made available by CM1BWC16Dbz");
        id += 1;
        blk+= 1;
      }
    }
    first_id += c*n*d;
  }

  currentIteration++;

//  currentIteration += 5;

  double res;
  TIMER_GET_MINE("CM1BWC16ReadBlksBIL", &res, attributes.GetCurrentTimeStep())
  LOG_TIMER_INFO(res << " : CM1BWC16Dbz::NextIteration() : TimeStep " 
    << attributes.GetCurrentTimeStep());
  return 0;
}
#endif

// Use Ceph to read iteration data files as objects
#ifdef USE_CEPH_FILE
int CM1BWC16Dbz::NextIterationCephFile(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
//  TIMER_SET("CM1BWC16IterCephFile");

  // TOFIX dont need the -1? do range check
	if (currentIteration == numIterations)
    return 1;

  LOG_CM1BWC16_INFO("CM1BWC16Dbz CephFile loading data for iteration " << currentIteration
        << "(" << this->iters[currentIteration]  << ") ...");

  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

//  TOFIX
//  int procNbrPerXYDim = sqrt(mpiSize);
//  int blockNbrPerXYDimProc = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/procNbrPerXYDim;
  int n = sqrt(mpiSize);
  int c = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/n; // blocks in this process along the x or y dimension

//  TOFIX
//  //int procExtents[6];
//  //int xProcStartExtent = mpiRank%ProcNbrPerXYDim;
//  int nnx = mpiRank%procNbrPerXYDim;
//  int nny = mpiRank/procNbrPerXYDim;
//  int blockNbrPerZDim = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;
  int nnx = mpiRank % n;
  int nny = mpiRank / n;
  int d = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

  std::string dataFilePath = this->dsetPath + 
                        this->iters[this->currentIteration];
  int ret = 0;
  /* Declare the cluster handle and required variables. */
  rados_t cluster;
  char cluster_name[] = "ceph";
  char user_name[] = "lrahmani";
  uint64_t flags;
  /* Initialize the cluster handle with the "ceph" cluster name 
   * and "client.lrahmani" user */
    ret = rados_create(&cluster, (const char*) user_name);
    if (ret < 0) {
      std::cerr << "Couldn't initialize the cluster handle! error " 
        << ret << std::endl;
      return EXIT_FAILURE;
    } else {
      std::cout << "Created a cluster handle." << std::endl;
    }
  /* Read a Ceph configuration file to configure the cluster handle. */
    ret = rados_conf_read_file(cluster, "/home/lrahmani/.ceph/config");
    if (ret < 0) {
      std::cerr << "Couldn't read the Ceph configuration file! error " 
        << ret << std::endl;
      rados_shutdown(cluster);
      return EXIT_FAILURE;
    } else {
      std::cout << "Read the Ceph configuration file." << std::endl;
    }
  /* Connect to the cluster */
    ret = rados_connect(cluster);
    if (ret < 0) {
      std::cerr << "Couldn't connect to cluster! error " << ret << std::endl;
      rados_shutdown(cluster);
      return EXIT_FAILURE;
    } else {
      std::cout << "Connected to the cluster." << std::endl;
    }
  rados_ioctx_t io;
  char pool_name[] = "lrahmani_cm1-bw-c16";
  ret = rados_ioctx_create(cluster, pool_name, &io);
  if (ret < 0) {
    std::cerr << "Couldn't set up ioctx! error " << ret << std::endl;
    rados_ioctx_destroy(io);
    rados_shutdown(cluster);
    exit(EXIT_FAILURE);
  } else {
    std::cout << "Created an ioctx for  pool " << pool_name << std::endl;
  }

  int first_id = (nny*n*c+nnx)*c*d;
  size_t ttl_size = 0;
  int blk = 0;
  
  TIMER_SET("CM1BWC16ReadBlksCephFile");

  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
        int bid_tmp = grid.GetBlockAt(blk)->GetId();
        
        LOG_CM1BWC16_INFO("Updating block " << blk << "(" << bid_tmp 
          << " - simId " << id << ") ...");
        
        // TOFIX HUGE memory leak!!! Need to free previous arrays
        float* dbz = new float[NUM_PTS_BLOCK_X*NUM_PTS_BLOCK_Y*NUM_PTS_BLOCK_Z];	
		
	      int start_z = id % d;
	      int start_x = (id/d) % (c*n);
	      int start_y = (id/d) / (c*n);

//	      start_z *= NUM_PTS_BLOCK_Z;
//	      start_x *= NUM_PTS_BLOCK_X;
//	      start_y *= NUM_PTS_BLOCK_Y;

//	      int end_x = start_x + NUM_PTS_BLOCK_X - 1;
//	      int end_y = start_y + NUM_PTS_BLOCK_Y - 1;
//	      int end_z = start_z + NUM_PTS_BLOCK_Z - 1;
	
        unsigned int count = NUM_PTS_BLOCK_X*NUM_PTS_BLOCK_Y*NUM_PTS_BLOCK_Z;
//        std::string blockN("05364-dbz-data-all");
        std::string blockN("/data/lrahmani_787899/cm1-bw-c16/z_first-dbz/"); //TOFIX re-store files
        blockN += (this->iters[this->currentIteration]);
        
        
        // Read block'ss data
        int offset = start_y*(n*c*d)*count + start_x*(d)*NUM_PTS_BLOCK_Z*NUM_PTS_BLOCK_X +
          start_z*NUM_PTS_BLOCK_Z;
        float* offset_dbz = dbz;
        
        // TOFIX: Ceph performance doesn't like it ... at all!!

        for (int a=0 ; a<NUM_PTS_BLOCK_Y ; a++) {
          for (int b=0 ; b<NUM_PTS_BLOCK_X; b++){
            ret = rados_read(io, blockN.c_str(), (char*) offset_dbz, NUM_PTS_BLOCK_Z, offset);
            if (ret < 0) {
              std::cerr << "Couldn't start read object "<< blockN.c_str() 
                <<"! error " << ret << std::endl;
              rados_ioctx_destroy(io);
              rados_shutdown(cluster);
              exit(EXIT_FAILURE);
            } else {
//              std::cout << "Read object : " << blockN << " offset: " 
//                << offset << " count: " << count << std::endl;
            }
            offset += d*NUM_PTS_BLOCK_Z;
            offset_dbz += NUM_PTS_BLOCK_Z;
          }
          offset += (n*c*d)*NUM_PTS_BLOCK_X*NUM_PTS_BLOCK_Z;
        }
        
        

        // end of function
        attributes.UpdateDbz(dbz, bid_tmp);
        attributes.UpdateUinterp(dbz, bid_tmp);
        attributes.UpdateVinterp(dbz, bid_tmp);
        attributes.SetTimeStep(std::stoi(this->iters[this->currentIteration]));
        
        LOG_CM1BWC16_DEBUG("Data @" << dbz << "-" 
          << attributes.GetDbzArray(bid_tmp) 
          << "of Block " << blk << " (" << bid_tmp 
          << " - simId " << id << ") Iteration " << attributes.GetCurrentTimeStep() 
          << " made available by CM1BWC16Dbz");
        
        id += 1;
        blk+= 1;
      }
    }
    first_id += c*n*d;
  }


// Close connection to Ceph
//#ifdef USE_CEPH
//  rados_ioctx_destroy(io);
//  rados_shutdown(cluster);
//#endif

  currentIteration++;

  double res;
  TIMER_GET_MINE("CM1BWC16ReadBlksCephFile", &res, attributes.GetCurrentTimeStep())
  LOG_TIMER_INFO(res << " : CM1BWC16Dbz::NextIterationCephFile() : TimeStep " 
    << attributes.GetCurrentTimeStep());
  return 0;
}
#endif

// Use Ceph to read blocks data objects
#ifdef USE_CEPH_OBJECTS
int CM1BWC16Dbz::NextIterationCephObjects(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
  // TOFIX how to get the number of iterations
//	if (currentIteration == numIterations)
//    return 1;

  LOG_CM1BWC16_INFO("CM1BWC16Dbz CephObjects loading data for iteration " << currentIteration
        << "(" << this->iters[currentIteration]  << ") ...");

  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

  int n = sqrt(mpiSize);
  int c = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/n; // blocks in this process along the x or y dimension
  
  int nnx = mpiRank % n;
  int nny = mpiRank / n;
  int d = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

  std::string dataFilePath = this->dsetPath + 
                        this->iters[this->currentIteration];

//// Get Ceph ready
//#ifdef USE_CEPH
  int ret = 0;
  /* Declare the cluster handle and required variables. */
  rados_t cluster;
  char cluster_name[] = "ceph";
  char user_name[] = "lrahmani";
  uint64_t flags;
  /* Initialize the cluster handle with the "ceph" cluster name 
   * and "client.lrahmani" user */
    ret = rados_create(&cluster, (const char*) user_name);
    if (ret < 0) {
//      std::cerr << "Couldn't initialize the cluster handle! error " 
//        << ret << std::endl;
      return EXIT_FAILURE;
    } else {
//      std::cout << "Created a cluster handle." << std::endl;
    }
  /* Read a Ceph configuration file to configure the cluster handle. */
    ret = rados_conf_read_file(cluster, "/home/lrahmani/.ceph/config");
    if (ret < 0) {
//      std::cerr << "Couldn't read the Ceph configuration file! error " 
//        << ret << std::endl;
      rados_shutdown(cluster);
      return EXIT_FAILURE;
    } else {
//      std::cout << "Read the Ceph configuration file." << std::endl;
    }
  /* Connect to the cluster */
    ret = rados_connect(cluster);
    if (ret < 0) {
//      std::cerr << "Couldn't connect to cluster! error " << ret << std::endl;
      rados_shutdown(cluster);
      return EXIT_FAILURE;
    } else {
//      std::cout << "Connected to the cluster." << std::endl;
    }
  rados_ioctx_t io;
  char pool_name[] = "lrahmani_cm1bwc16dbz-blks";
  ret = rados_ioctx_create(cluster, pool_name, &io);
  if (ret < 0) {
//    std::cerr << "Couldn't set up ioctx! error " << ret << std::endl;
    rados_ioctx_destroy(io);
    rados_shutdown(cluster);
    exit(EXIT_FAILURE);
  } else {
//    std::cout << "Created an ioctx for the pool." << std::endl;
  }

  int first_id = (nny*n*c+nnx)*c*d;
  size_t ttl_size = 0;
  int blk = 0;
  
  TIMER_SET("CM1BWC16ReadBlksCephObjects");
  // TOFIX can make it very simple if changing blocks distribution
  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
        int bid_tmp = grid.GetBlockAt(blk)->GetId();
        
        LOG_CM1BWC16_INFO("Updating block " << blk << "(" << bid_tmp 
          << " - simId " << id << ") ...");

	      int start_z = id % d;
	      int start_x = (id/d) % (c*n);
	      int start_y = (id/d) / (c*n);

	      start_z *= NUM_PTS_BLOCK_Z;
	      start_x *= NUM_PTS_BLOCK_X;
	      start_y *= NUM_PTS_BLOCK_Y;

	      int end_x = start_x + NUM_PTS_BLOCK_X - 1;
	      int end_y = start_y + NUM_PTS_BLOCK_Y - 1;
	      int end_z = start_z + NUM_PTS_BLOCK_Z - 1;
        
        // compute ghost zones
	      if(start_x != 0)  { start_x -= 1; }
	      if(start_y != 0)  { start_y -= 1; }
	      if(start_z != 0)  { start_z -= 1; }

        // TOFIX HUGE memory leak!!! Need to free previous arrays
        unsigned int count = (end_x-start_x+1)*(end_y-start_y+1)*(end_z-start_z+1);
        float* dbz = new float[count];	

// Use Ceph to read data one object for each block 
//#ifdef CEPH_R_BLOCKS
        std::string blockN = "0"; //TOFIX
        blockN += std::to_string(std::stoi(this->iters[this->currentIteration]));
        blockN += "-dbz-zfirst-";
        blockN += std::to_string(id);
        ret = rados_read(io, blockN.c_str(), (char*) dbz, count*sizeof(float), 0);
        if (ret < 0) {
          //std::cerr << "Couldn't start read object "<< blockN.c_str() 
          //  <<"! error " << ret << std::endl;
          rados_ioctx_destroy(io);
          rados_shutdown(cluster);
          exit(EXIT_FAILURE);
        } else {
          //std::cout << "Read object : " << blockN << std::endl;
        }

        attributes.UpdateDbz(dbz, bid_tmp);
        attributes.UpdateUinterp(dbz, bid_tmp);
        attributes.UpdateVinterp(dbz, bid_tmp);
        attributes.SetTimeStep(std::stoi(this->iters[this->currentIteration]));
        
        LOG_CM1BWC16_DEBUG("Data @" << dbz << "-" 
          << attributes.GetDbzArray(bid_tmp) 
          << "of Block " << blk << " (" << bid_tmp 
          << " - simId " << id << ") Iteration " << attributes.GetCurrentTimeStep() 
          << " made available by CM1BWC16Dbz - Ceph");
        id += 1;
        blk+= 1;
      }
    }
    first_id += c*n*d;
  }


// Close connection to Ceph
//#ifdef USE_CEPH
//  rados_ioctx_destroy(io);
//  rados_shutdown(cluster);
//#endif

  currentIteration++;

  double res;
  TIMER_GET_MINE("CM1BWC16ReadBlksCephObjects", &res, attributes.GetCurrentTimeStep())
  LOG_TIMER_INFO(res << " : CM1BWC16Dbz::NextIterationCephObjects() : TimeStep " 
    << attributes.GetCurrentTimeStep());
  return 0;
}
#endif

// Store read blocks' data using MPI I/O (or CephFile?) an store them as objects
#ifdef USE_CEPH_STORE
int CM1BWC16Dbz::NextIterationCephStore(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
  // TOFIX dont need the -1? do range check
	if (currentIteration == numIterations)
    return 1;
  
  LOG_CM1BWC16_INFO("CM1BWC16Dbz CephStore loading data for iteration  " 
    << currentIteration << "(" << this->iters[currentIteration]  << ") using MPIO ...");

  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

  int n = sqrt(mpiSize);
  int c = (NUM_PTS_GLOBAL_X/NUM_PTS_BLOCK_X)/n; // blocks in this process along the x or y dimension

  int nnx = mpiRank % n;
  int nny = mpiRank / n;
  int d = NUM_PTS_GLOBAL_Z/NUM_PTS_BLOCK_Z;

  std::string dataFilePath = this->dsetPath + 
                        this->iters[this->currentIteration];
// Get MPI IO ready
//#ifdef USE_MPI_IO
	MPI_File fh;
  LOG_CM1BWC16_DEBUG("Opening file " << dataFilePath.c_str() << " ...");
  MPI_File_open(MPI_COMM_SELF, dataFilePath.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  // TOFIX test for open failure 
//#endif

  // Get Ceph ready
  int ret = 0;
  /* Declare the cluster handle and required variables. */
  rados_t cluster;
  char cluster_name[] = "ceph";
  char user_name[] = "lrahmani";
  uint64_t flags;
  /* Initialize the cluster handle with the "ceph" cluster name 
   * and "client.lrahmani" user */
    ret = rados_create(&cluster, (const char*) user_name);
    if (ret < 0) {
      LOG_CM1BWC16_ERROR("Couldn't initialize the cluster handle error " << ret);
//      std::cerr << "Couldn't initialize the cluster handle! error " 
//        << ret << std::endl;
      return EXIT_FAILURE; // TOFIX LOG_ERROR should exit
    } else {
      LOG_CM1BWC16_DEBUG("Created a cluster handle");
    }
  /* Read a Ceph configuration file to configure the cluster handle. */
    ret = rados_conf_read_file(cluster, "/home/lrahmani/.ceph/config");
    if (ret < 0) {
      LOG_CM1BWC16_ERROR("Couldn't read the Ceph configuration file error "  << ret);
      rados_shutdown(cluster);
      return EXIT_FAILURE;
    } else {
      LOG_CM1BWC16_DEBUG("Read the Ceph configuration file");
    }
  /* Connect to the cluster */
    ret = rados_connect(cluster);
    if (ret < 0) {
      LOG_CM1BWC16_ERROR("Couldn't connect to cluster! error " << ret);
      rados_shutdown(cluster);
      return EXIT_FAILURE;
    } else {
      LOG_CM1BWC16_DEBUG("Connected to the cluster");
    }
  rados_ioctx_t io;
  char pool_name[] = "lrahmani_cm1bwc16dbz-blks";
  ret = rados_ioctx_create(cluster, pool_name, &io);
  if (ret < 0) {
    LOG_CM1BWC16_ERROR("Couldn't set up ioctx! error " << ret);
    rados_ioctx_destroy(io);
    rados_shutdown(cluster);
    exit(EXIT_FAILURE);
  } else {
    LOG_CM1BWC16_DEBUG("Created an ioctx for the pool");
  }

  int first_id = (nny*n*c+nnx)*c*d;
  size_t ttl_size = 0;
  int blk = 0;
  
  TIMER_SET("CM1BWC16ReadBlksCephStore");

  for(int i=0; i < c; i++) {
  int id = first_id;
    for(int j=0; j < c; j++) {
      for(int k=0; k < d; k++) {
        int bid_tmp = grid.GetBlockAt(blk)->GetId();
        
        LOG_CM1BWC16_DEBUG("Updating block " << blk << "(" << bid_tmp 
          << " - simId " << id << ") ...");
        
	      int start_z = id % d;
	      int start_x = (id/d) % (c*n);
	      int start_y = (id/d) / (c*n);

	      start_z *= NUM_PTS_BLOCK_Z;
	      start_x *= NUM_PTS_BLOCK_X;
	      start_y *= NUM_PTS_BLOCK_Y;

	      int end_x = start_x + NUM_PTS_BLOCK_X - 1;
	      int end_y = start_y + NUM_PTS_BLOCK_Y - 1;
	      int end_z = start_z + NUM_PTS_BLOCK_Z - 1;
        
        // compute ghost zones
	      if(start_x != 0)  { start_x -= 1; }
	      if(start_y != 0)  { start_y -= 1; }
	      if(start_z != 0)  { start_z -= 1; }

        int size_big[3];
        size_big[0] = 2200;//NUM_PTS_GLOBAL_X;
        size_big[1] = 2200;//NUM_PTS_GLOBAL_Y;
        size_big[2] = 380;//NUM_PTS_GLOBAL_Z;
	
        int size_small[3];
        size_small[0] = end_x - start_x + 1;
        size_small[1] = end_y - start_y + 1;
        size_small[2] = end_z - start_z + 1;

        int offsets[3];
        offsets[0] = start_x;
        offsets[1] = start_y;
        offsets[2] = start_z;
        
        // TOFIX HUGE memory leak!!! Need to free previous arrays
        float* dbz = new float[(end_x-start_x+1)*(end_y-start_y+1)*(end_z-start_z+1)];	

// Use MPI IO to read blocks data for the iteration data file
//#ifdef USE_MPI_IO
	      MPI_Datatype cube;
        MPI_Type_create_subarray(3,size_big,size_small,
                offsets,MPI_ORDER_C,MPI_FLOAT,&cube);

        unsigned int count = (end_x-start_x+1)*(end_y-start_y+1)*(end_z-start_z+1);

        MPI_Type_commit(&cube);

        MPI_File_set_view(fh, 0, MPI_FLOAT, cube, "native", MPI_INFO_NULL);
        MPI_Status status;
        int err = MPI_File_read(fh, dbz, count, MPI_FLOAT, &status);
        
        MPI_Type_free(&cube);
//#endif
        // TOFIX no need to save data as it will not be visualized
        //       actually it's useful to check the validaty of stored data
        attributes.UpdateDbz(dbz, bid_tmp);
        attributes.UpdateUinterp(dbz, bid_tmp);
        attributes.UpdateVinterp(dbz, bid_tmp);
        attributes.SetTimeStep(std::stoi(this->iters[this->currentIteration]));

        // Ask Ceph to write data blocks as objects
        /* Write an object synchronously. */
        std::string blockN = "0"; //TOFIX
        blockN += std::to_string(attributes.GetCurrentTimeStep());
        blockN += "-dbz-zfirst-";
        blockN += std::to_string(id);
        ret = rados_write_full(io, blockN.c_str(), (char*) dbz, count*sizeof(float));
        if (ret < 0) {
          LOG_CM1BWC16_ERROR("Couldn't write object! error " << ret);
          rados_ioctx_destroy(io);
          rados_shutdown(cluster);
          exit(EXIT_FAILURE);
        } else {
          LOG_CM1BWC16_INFO("Wrote new object " << blockN ); //TOFIX change to debug level
        } 
        
//        LOG_CM1BWC16_DEBUG("Data @" << dbz << "-" 
//          << attributes.GetDbzArray(bid_tmp) 
//          << "of Block " << blk << " (" << bid_tmp 
//          << " - simId " << id << ") Iteration " << attributes.GetCurrentTimeStep() 
//          << " made available by CM1BWC16Dbz");
        id += 1;
        blk+= 1;
      }
    }
    first_id += c*n*d;
  }

// Close iteration's data file 
//#ifdef USE_MPI_IO
  MPI_File_close(&fh);
//#endif

// Close connection to Ceph
//#ifdef USE_CEPH
//  rados_ioctx_destroy(io);
//  rados_shutdown(cluster);
//#endif

  currentIteration++;

  double res;
  TIMER_GET_MINE("CM1BWC16ReadBlksCephStore", &res, attributes.GetCurrentTimeStep())
  LOG_TIMER_INFO(res << " : CM1BWC16Dbz::NextIterationCephStore() : TimeStep " 
    << attributes.GetCurrentTimeStep());

  return 0;
}
#endif

