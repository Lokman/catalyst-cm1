#!/bin/bash
LOG_DIRNAME=./.logs_bash/
LOG_DIRNAME_ROOT=/root/.logs_bash/

# Log all bash history
mkdir -p ${LOG_DIRNAME}
export PS1='\[\033[00;44m\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$\[\033[00m\] '
export PROMPT_COMMAND='echo "$(date "+%Y-%m-%d.%H:%M:%S") $(pwd) $(history 1)" >> ${LOG_DIRNAME}/bash-history-$(date "+%Y-%m-%d").log'

# Get root bash history - as it will be lost by the end of g5k reservation
mkdir -p ${LOG_DIRNAME}/root/
scp bashrc_root root@$(hostname):~/.bashrc
alias save_root_logs="scp root@$(hostname):${LOG_DIRNAME_ROOT}/* ${LOG_DIRNAME}/root/"
