/*
 * Based on ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#include "CM1DataStructures.hpp"

#include <mpi.h>
#include <iostream>

Grid::Grid()
{
//  LOG_SET_COLOR_CM1DS(LOG_WHITE_TXT); 
//  LOG_SET_COLOR_TIMER(LOG_GREEN_BKG);
//  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
////  LOG_SET_LEVEL_CM1DS(LOG_NONE);
////  LOG_SET_LEVEL_WORKING_ON(LOG_NONE);
}


void Grid::Initialize(const unsigned int numPoints[3])
{
	if(numPoints[0] == 0 || numPoints[1] == 0 || numPoints[2] == 0)
	{
    std::cerr << "Must have a non-zero amount of points in each direction.\n";
	}
  
	for(int i=0;i<3;i++) {
    this->GlobalExtents[i*2] = 0;
    this->GlobalExtents[i*2+1] = numPoints[i]-1;
	}
	
	//
	blocksCounter = 0;
}

int* Grid::GetGlobalExtents(){
	return GlobalExtents;
}

// TOFIX for all methods that take blockId as an argument, 
// do range check!


unsigned int Grid::GetNumberOfPoints(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetNumberOfPoints();
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found ");	
		return -1;
	}
//  return (blocks[blockId]->GetExtents()[1]-blocks[blockId]->GetExtents()[0]+1) *
//    (blocks[blockId]->GetExtents()[3]-blocks[blockId]->GetExtents()[2]+1) *
//    (blocks[blockId]->GetExtents()[5]-blocks[blockId]->GetExtents()[4]+1);
}

unsigned int Grid::GetNumberOfCells(int blockId)
{
//  return (blocks[blockId]->GetExtents()[1]-blocks[blockId]->GetExtents()[0]) *
//    (blocks[blockId]->GetExtents()[3]-blocks[blockId]->GetExtents()[2]) *
//    (blocks[blockId]->GetExtents()[5]-blocks[blockId]->GetExtents()[4]);
	LOG_CM1DS_ERROR("You are using a function not implemented yet!!!");
	return -1;
}

// TOFIX to implement
unsigned int Grid::GetNumberOfAllPoints()
{
	LOG_CM1DS_ERROR("You are using a function not implemented yet!!!");
	return 1;
}
// TOFIX to implement
unsigned int Grid::GetNumberOfAllCells()
{
	LOG_CM1DS_ERROR("You are using a function not implemented yet!!!");
	return 1;
}

unsigned int* Grid::GetExtents(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
  	return block->GetExtents();
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return nullptr;
	}
}

//unsigned int Grid::GetXExtent()
//{
//	return this->Extents[1]-this->Extents[0]+1;
//}
//unsigned int Grid::GetYExtent()
//{
//	return this->Extents[3]-this->Extents[2]+1;
//}
//unsigned int Grid::GetZExtent()
//{
//	return this->Extents[5]-this->Extents[4]+1;
//}

float* Grid::GetXLocalCoord(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetXCoord();
	else { 
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return nullptr;
	}
}

float* Grid::GetYLocalCoord(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetYCoord();
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return nullptr;
	}
}

float* Grid::GetZLocalCoord(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetZCoord();
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return nullptr;
	}
}

// TOFIX use the return value to signal errors	
int Grid::SetXLocalCoord(float* coord, int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr){
		LOG_CM1DS_DEBUG("Setting new X coordinates [@" << coord << ",] for block id " << blockId);
		block->SetXCoord(coord);
		LOG_CM1DS_DEBUG("New X coordinates         [@" << GetXLocalCoord(blockId) << ",] for block id " << blockId);
		// TOFIX free the previous array?
		return 0;
	} else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}

// TOFIX find a more effective way to write 3D functions
// that have the same core
int Grid::SetYLocalCoord(float* coord, int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr) {
		LOG_CM1DS_DEBUG("Setting new Y coordinates [@" << coord << ",] for block id " << blockId);
		block->SetYCoord(coord);
		LOG_CM1DS_DEBUG("New Y coordinates         [@" << GetYLocalCoord(blockId) << ",] for block id " << blockId);
		// TOFIX free the previous array?
		return 0;
	} else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}
int Grid::SetZLocalCoord(float* coord, int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr) {
		// TOFIX use LOG_TEST instead
		LOG_CM1DS_DEBUG("Setting new Z coordinates [@" << coord << ",] for block id " << blockId);
		block->SetZCoord(coord);
		LOG_CM1DS_DEBUG("New Z coordinates         [@" << GetZLocalCoord(blockId) << ",] for block id " << blockId);
		// TOFIX free the previous array?
		return 0;
	} else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}

// TOFIX range check
// TOFIX use extents to compute DimSizes
// TOFIX solve the axises order
int Grid::GetXDimSize(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetXDimSize();
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}
int Grid::GetYDimSize(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetYDimSize();
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}
int Grid::GetZDimSize(int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr)
		return block->GetZDimSize();
	else {
		LOG_CM1DS_ERROR("Index " << blockId << " out of range " << blocks.size()-1);	
		return -1;
	}
}

// TOFIX SetXDim is also considered as AddXDim
// TOFIX redundent with extents
int Grid::SetXDimSize(int dimSize, int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr){
		LOG_CM1DS_DEBUG("Setting new X dim size " << dimSize << " for block id " << blockId);
		block->SetXDimSize(dimSize);
		LOG_CM1DS_DEBUG("New X dim size         " << GetXDimSize(blockId) << " for block id " << blockId);
		return 0;
	} else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}
int Grid::SetYDimSize(int dimSize, int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr){
		LOG_CM1DS_DEBUG("Setting new Y dim size " << dimSize << " for block id " << blockId);
		block->SetYDimSize(dimSize);
		LOG_CM1DS_DEBUG("New Y dim size         " << GetYDimSize(blockId) << " for block id " << blockId);
		return 0;
	} else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}
int Grid::SetZDimSize(int dimSize, int blockId)
{
	Block* block = this->GetBlock(blockId);
	if (block != nullptr){
		LOG_CM1DS_DEBUG("Setting new Z dim size " << dimSize << " for block id " << blockId);
		block->SetZDimSize(dimSize);
		LOG_CM1DS_DEBUG("New Z dim size         " << GetZDimSize(blockId) << " for block id " << blockId);
		return 0;
	} else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return -1;
	}
}

int Grid::GetNumberOfLocalBlocks()
{
		return blocks.size(); 
}
	
int Grid::GetNumberofAllBlocks()
{
	LOG_CM1DS_ERROR("You are calling a function that is not implemented yet!!!");
	return -1;
}

int Grid::CreateBlock(const unsigned int numLocalPoints[3])
{
	Block* block = new Block(numLocalPoints);
	blocks.push_back(block);
	blocksCounter++;
	int position = blocksCounter;
	int mpiSize = 1;
	int mpiRank = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	// TOFIX block identifier as seperate function
	std::string strId;
	strId = "rank"+std::to_string(mpiRank)+"size"+std::to_string(mpiSize)+"block"+std::to_string(position)+"size"+std::to_string(blocks.size());
	int intId = std::hash<std::string>{}(strId); 
	block->SetId(intId);
	blockWithId[intId] = block;
	LOG_CM1DS_DEBUG("New block created at @" << &(blocks[0])+position << " with id " << block->GetId());
	return block->GetId();
}

int Grid::CreateBlock()
{
	Block* block = new Block();
	// TOFIX redundent with previous from next line till the end
	blocks.push_back(block);
	blocksCounter++;
	int position = blocksCounter;
	int mpiSize = 1;
	int mpiRank = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	// TOFIX block identifier as seperate function
	std::string strId;
	strId= "rank"+std::to_string(mpiRank)+"size"+std::to_string(mpiSize)+"block"+std::to_string(position)+"size"+std::to_string(blocks.size());
	int intId = std::hash<std::string>{}(strId);
	block->SetId(intId);
	blockWithId[intId] = block;
	LOG_CM1DS_DEBUG("New block created at @" << &(blocks[0])+position << " with id " << block->GetId());
	return block->GetId();
}

// TOFIX const pointer
//const Block* Grid::GetBlock(int blockId)
Block* Grid::GetBlock(int blockId)
{
	std::unordered_map<int, Block*>::const_iterator result = blockWithId.find(blockId);
	if (result != blockWithId.end())
		return result->second;
	else {
		LOG_CM1DS_ERROR("Block " << blockId << " not found in the hash table");
		return nullptr;
	}
}

Block* Grid::GetBlockAt(int index)
{
	if (index < blocks.size()){
		return blocks[index];
	} else {
		LOG_CM1DS_ERROR("Index " << index << " out of range ([0," << blocks.size()-1 << "])");
		return nullptr;
	}
}

Attributes::Attributes()
{
  this->GridPtr = NULL;
}

void Attributes::Initialize(Grid* grid)
{
  this->GridPtr = grid;
}
// TOFIX also used as set and add dbz
// TOFIX change to int (float*, int)
int Attributes::UpdateDbz(float* data, int blockId)
{
	//const Block* block = GridPtr->GetBlock(blockId);
	Block* block = GridPtr->GetBlock(blockId);
	if (block != nullptr) {
		// TOFIX Attrbutes free the previous mem? dont think so
		// However I can force the use of shared ptrs
		//  Check legacy issues
		if (block->GetDbzArray())
    {
    LOG_CM1DS_DEBUG("Updating dbz data array @" << data << " for block id " << blockId);
		free(block->GetDbzArray());
    block->UpdateDbz(data);
		LOG_CM1DS_DEBUG("Dbz data array updated  @" << GetDbzArray(blockId) << " for block id " << blockId);
    return 0;
    } else
    {
		// TOFIX this function set and update dbz
		LOG_CM1DS_DEBUG("Setting new dbz data array @" << data << " for block id " << blockId);
		block->UpdateDbz(data);
		LOG_CM1DS_DEBUG("New dbz data array         @" << GetDbzArray(blockId) << " set for block id " << blockId);
		return 0;
    }
	} else {
    LOG_CM1DS_DEBUG("Block " << blockId << " not found");
    return 1;
  }
}

int Attributes::UpdateUinterp(float* data, int blockId)
{
	//const Block* block = GridPtr->GetBlock(blockId);
	Block* block = GridPtr->GetBlock(blockId);
	if (block != nullptr) {
		// TOFIX Attrbutes free the previous mem? dont think so
		// However I can force the use of shared ptrs
		//  Check legacy issues
		if (block->GetUinterpArray())
    {
    LOG_CM1DS_DEBUG("Updating uinterp data array @" << data << " for block id " << blockId);
		// TOFIX: commented to avoid multiple mem free
    //free(block->GetUinterpArray());
    block->UpdateUinterp(data);
		LOG_CM1DS_DEBUG("Uinterp data array updated  @" << GetUinterpArray(blockId) << " for block id " << blockId);
    return 0;
    } else
    {
		// TOFIX this function set and update uinterp
		LOG_CM1DS_DEBUG("Setting new uinterp data array @" << data << " for block id " << blockId);
		block->UpdateUinterp(data);
		LOG_CM1DS_DEBUG("New uinterp data array         @" << GetUinterpArray(blockId) << " set for block id " << blockId);
		return 0;
    }
	} else {
    LOG_CM1DS_DEBUG("Block " << blockId << " not found");
    return 1;
  }
}

int Attributes::UpdateVinterp(float* data, int blockId)
{
	//const Block* block = GridPtr->GetBlock(blockId);
	Block* block = GridPtr->GetBlock(blockId);
	if (block != nullptr) {
		// TOFIX Attrbutes free the previous mem? dont think so
		// However I can force the use of shared ptrs
		//  Check legacy issues
		if (block->GetVinterpArray())
    {
    LOG_CM1DS_DEBUG("Updating vinterp data array @" << data << " for block id " << blockId);
		// TOFIX: commented to avoid multiple mem free
    //free(block->GetVinterpArray());
    block->UpdateVinterp(data);
		LOG_CM1DS_DEBUG("Vinterp data array updated  @" << GetVinterpArray(blockId) << " for block id " << blockId);
    return 0;
    } else
    {
		// TOFIX this function set and update vinterp
		LOG_CM1DS_DEBUG("Setting new vinterp data array @" << data << " for block id " << blockId);
		block->UpdateVinterp(data);
		LOG_CM1DS_DEBUG("New vinterp data array         @" << GetVinterpArray(blockId) << " set for block id " << blockId);
		return 0;
    }
	} else {
    LOG_CM1DS_DEBUG("Block " << blockId << " not found");
    return 1;
  }
}

void Attributes::SetTimeStep(int timeStep)
{
	this->timeStep = timeStep;
}

float* Attributes::GetDbzArray(int blockId)
{
	Block* block = GridPtr->GetBlock(blockId);
	if (block != NULL)
		return block->GetDbzArray();
	else
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return NULL;
}

float* Attributes::GetUinterpArray(int blockId)
{
	Block* block = GridPtr->GetBlock(blockId);
	if (block != NULL)
		return block->GetUinterpArray();
	else
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return NULL;
}

float* Attributes::GetVinterpArray(int blockId)
{
	Block* block = GridPtr->GetBlock(blockId);
	if (block != NULL)
		return block->GetVinterpArray();
	else
		LOG_CM1DS_ERROR("Block " << blockId << " not found");
		return NULL;
}

int Attributes::GetCurrentTimeStep()
{
	return timeStep;
}

// TOFIX consider timestep too, when it is fully implemented
Block::Block()
{
	for (int i=0 ; i<6 ; i++ )
		Extents[i] = 0;
	xLocalCoord = nullptr;
	yLocalCoord = nullptr;
	zLocalCoord = nullptr;

	dbz = nullptr;
	vinterp = nullptr;
	uinterp = nullptr;

	currentTimeStep = 0;
}
// TOFIX allow extents to start from non-zero value
Block::Block(const unsigned int numPoints[3])
{
	Block();
	for (int i=0 ; i<3 ; i++ ){
		Extents[2*i] = 0;
		Extents[2*i+1] = numPoints[i]-1;
	}
}

int Block::GetId()
{
	return id;
}

void Block::SetId(int blockId)
{
	this->id = blockId;
}

// Grid related members
unsigned int Block::GetNumberOfPoints()
{
  return (Extents[1]-Extents[0]+1) *
    (Extents[3]-Extents[2]+1) *
    (Extents[5]-Extents[4]+1);
}
unsigned int Block::GetNumberOfCells()
{
	LOG_CM1DS_ERROR("You are calling a function that is not implemented yet!!!");
	return -1;
}

float* Block::GetXCoord()
{
	return xLocalCoord;
}
float* Block::GetYCoord()
{
	return yLocalCoord;
}
float* Block::GetZCoord()
{
	return zLocalCoord;
}

// TOFIX free the previous array
void Block::SetXCoord(float* coord)
{
	xLocalCoord = coord;
}
void Block::SetYCoord(float* coord)
{
	yLocalCoord = coord;
}
void Block::SetZCoord(float* coord)
{
	zLocalCoord = coord;
}

int Block::GetXDimSize()
{
	return Extents[1]-Extents[0]+1;
}
int Block::GetYDimSize()
{
	return Extents[3]-Extents[2]+1;
}
int Block::GetZDimSize()
{
	return Extents[5]-Extents[4]+1;
}
// TOFIX redundent with extents?
void Block::SetXDimSize(int dimSize)
{
		Extents[0] = 0;
		Extents[1] = dimSize-1;
}
void Block::SetYDimSize(int dimSize)
{
		Extents[2] = 0;
		Extents[3] = dimSize-1;
}
void Block::SetZDimSize(int dimSize)
{
		Extents[4] = 0;
		Extents[5] = dimSize-1;
}

// TOFIX const int GetExtents()?
unsigned int* Block::GetExtents()
{
	return Extents;
}

// Attributes related members
// TOFIX free the previous pointer
void Block::UpdateDbz(float* data)
{
	dbz = data;
}
void Block::UpdateUinterp(float* data)
{
	uinterp = data;
}
void Block::UpdateVinterp(float* data)
{
	vinterp = data;
}

float* Block::GetDbzArray()
{
	return dbz;
}
float* Block::GetUinterpArray()
{
	return uinterp;
}
float* Block::GetVinterpArray()
{
	return vinterp;
}

int Block::GetCurrentTimeStep()
{
	LOG_CM1DS_ERROR("You are calling a function that is not implemented yet!!!");
	return -1;
}



