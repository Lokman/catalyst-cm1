" Default G5k vim config /usr/share/vim/vimrc
syntax on
set nocompatible
set showcmd
set showmatch
"set ignorecase
"set smartcase
set incsearch
set background=dark
set backspace=indent,eol,start
" status bar configuration
set ruler
set laststatus=2
set statusline=%<%f%h%m%r%=%l,%c\ %P
" highlight searched items
set hlsearch
" go to next/previous line with left/right arrow
set whichwrap=<,>,[,]


" Regular Coding config
"filetype plugin indent on
syntax on 
set number autoindent tabstop=2 expandtab incsearch hlsearch

" Bash-like editing shortcut
inoremap <C-e> <Esc>A
inoremap <C-a> <Esc>I

" 80 chars column lenght
"highlight OverLength ctermbg=red ctermfg=white guibg=#592929
"match OverLength /\%81v.\+/
" For vim >= 7.3
set colorcolumn=80 

" Useful for querty layouts
command W w
command Q q

" Macros to comment and uncomment code with range
command -range CommCPP <line1>,<line2>:s!^!//!g | :noh
command -range CommCPPRm <line1>,<line2>:s!^//!!g | :noh
command -range CommPy <line1>,<line2>:s!^!#!g | :noh
command -range CommentPyRm <line1>,<line2>:s!^#!!g | :noh

" Google CPP indent file
"source cpp_google.vim

