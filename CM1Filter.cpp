#include <iostream>
#include <mpi.h>

#include "CM1Filter.hpp"
#include "mpi_mclog_m4.hpp"

// TOFIX
#include <cstdlib>
#include <ctime>
#include <algorithm>

std::vector<score_t> compute_scores(Grid& grid, Attributes& attributes);
float get_variability_score(Block* block);
int sort_blocks_scores(std::vector<score_t>& blks_scores);
int sort_blocks_scores_glb(std::vector<score_t>& blks_scores, Grid& grid, Attributes& attributes);
std::vector<score_t> get_blocks_to_reduce(std::vector<score_t>& blks_scores);
std::vector<score_t> get_blocks_to_reduce_glb(std::vector<score_t>& blks_scores,
  Grid& grid, Attributes& attributes);
int reduce_blocks(std::vector<score_t>& blks, Grid& grid, Attributes& attributes, bool use_mask);

float Filter::percentage = 0;
//std::vector<score_t> Filter::blks_scores; // TOFIX Need to free pointers
std::string Filter::basename = "scores";

int Filter::Initialize(float percentage)
{
  Filter::percentage = percentage;
  LOG_WORKING_ON_INFO("Filtering system initialized with percentage " 
    << Filter::percentage);
  return 0;
}

int Filter::Filter(Grid& grid, Attributes& attributes)
{
  // compute scores
  std::vector<score_t> blks_scores = compute_scores(grid, attributes);

  // sort scores
  sort_blocks_scores_glb(blks_scores, grid, attributes);
  
  // select blocks to reduce
  std::vector<score_t> to_reduce = get_blocks_to_reduce_glb(blks_scores, grid, attributes);

  // reduce blocks
  reduce_blocks(to_reduce, grid, attributes, true);
 
  return 0;
}

int Filter::Finalize()
{
  return 0;
}

// Helper functions
std::vector<score_t> compute_scores(Grid& grid, Attributes& attributes)
{
	srand ((unsigned int) time(0));// TOFIX  

  std::vector<score_t> blks_scores;
  int numBlocks = grid.GetNumberOfLocalBlocks();
	for ( int blk=0 ; blk < numBlocks ; blk++){
		Block* block = grid.GetBlockAt(blk);
		int bid = block->GetId();
    float score = get_variability_score(block);
    score_t blk_score(bid,score);
    blks_scores.push_back(blk_score);
    LOG_WORKING_ON_DEBUG("Score " << blks_scores.back().score << " of block "
      << blks_scores.back().id << " registred");
  }

  return blks_scores;
}

float get_variability_score(Block* block){
  return (static_cast <float> (rand()) / (static_cast <float> (RAND_MAX)))*100;
}

int sort_blocks_scores(std::vector<score_t>& blks_scores)
{
  std::sort(blks_scores.begin(), blks_scores.end());
  string ordered_scores ("");
  for (const auto &blk_score : blks_scores){
    ordered_scores+= std::to_string(blk_score.score);
    ordered_scores+= "  ";
  }
  LOG_WORKING_ON_DEBUG("Sorted (" << blks_scores.size() << ") blocks: " 
    << ordered_scores);
  return 0;
}

std::vector<score_t> get_blocks_to_reduce(std::vector<score_t>& blks_scores)
{
  std::vector<score_t> to_reduce;
  int blks_num = static_cast<int> (blks_scores.size()*Filter::percentage);
  
  LOG_WORKING_ON_DEBUG("Selecting " << blks_num 
    << " blocks to be reduced for percentage " << Filter::percentage);

  for(const auto &blk_score : blks_scores){
    blks_num--;
    if (blks_num < 0) 
      break;
    to_reduce.push_back(blk_score);
  }
  
  LOG_WORKING_ON_DEBUG(to_reduce.size() << " blocks registred to be reduced");

  return to_reduce;
}

// sort blocks scores globally - sorts all iteration blocks
int sort_blocks_scores_glb(std::vector<score_t>& blks_scores, Grid& grid, Attributes& attributes)
{
  // gather all scores at rank 0

  int blks_scores_count = sizeof(score_t)*blks_scores.size();
  // TOFIX be aware of blocks number per-process change after loadbalancing

  int mpiSize = 1;
  int mpiRank = 0;
  
  // get process rank and world size
  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
  
  // all blocks scores vector
  std::vector<score_t> blks_scores_glb;

  if(mpiRank == 0){
    blks_scores_glb.resize(mpiSize*blks_scores.size()); 
    // TOFIX be aware of load balancing
  }
  
  // gather all scores at rank 0
  MPI_Gather(&blks_scores[0],blks_scores_count,MPI_BYTE,&blks_scores_glb[0],
    blks_scores_count,MPI_BYTE,0,MPI_COMM_WORLD);
  
  // sort all received blocks at rank 0
  if(mpiRank == 0){
    std::sort(blks_scores_glb.begin(), blks_scores_glb.end());
    std::string ordered_scores ("");
    for (const auto &blk_score : blks_scores_glb){
      ordered_scores+= std::to_string(blk_score.score);
      ordered_scores+= "  ";
    }
//    LOG_WORKING_ON_DEBUG("Sorted (" << blks_scores_glb.size() << ") blocks: " 
//      << ordered_scores);
    
    LOG_WORKING_ON_DEBUG("Sorted (" << blks_scores_glb.size() << ") blocks: (" 
      << blks_scores_glb[0].id << "," << blks_scores_glb[0].score << ") -> ("
      << blks_scores_glb[blks_scores_glb.size()-1].id << "," 
      << blks_scores_glb[blks_scores_glb.size()-1].score << ")");
    
    // save sorted blocks to file
    std::string filename(Filter::basename);
    filename += "-";
    filename += std::to_string(attributes.GetCurrentTimeStep());
    LOG_WORKING_ON_DEBUG("Saving sorted blocks to file " << filename << " ...");
    std::ofstream ofs(filename);
    ofs.write((char*)(&blks_scores_glb[0]), blks_scores_glb.size()*sizeof(score_t));
    LOG_WORKING_ON_INFO(blks_scores_glb.size() 
      << " blocks sorted and saved successfuly to file " << filename <<" (by rank 0)");
  }

  MPI_Barrier(MPI_COMM_WORLD);
  return 0;
}

std::vector<score_t> get_blocks_to_reduce_glb(std::vector<score_t>& blks_scores,
  Grid& grid, Attributes& attributes)
{
  // get local blocks to be reduced
  
  int mpiSize = 1;
  int mpiRank = 0;

  MPI_Comm_size(MPI_COMM_WORLD,&mpiSize);
  MPI_Comm_rank(MPI_COMM_WORLD,&mpiRank);
  
  std::vector<score_t> to_reduce;
  int blks_num = static_cast<int> (blks_scores.size()*Filter::percentage);
  
  std::vector<score_t> blks_scores_glb;
  int blks_scores_glb_count = 0;
  // read iteration score file
  if(mpiRank == 0){
    std::string filename(Filter::basename);
    filename+= "-";
    filename+= std::to_string(attributes.GetCurrentTimeStep());
    
    LOG_WORKING_ON_DEBUG("Loading iteration score file " << filename 
      << " ... - (only rank 0)");

    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    // get scores count
    blks_scores_glb_count = in.tellg()/sizeof(score_t);
    // read scores
    blks_scores_glb.resize(blks_scores_glb_count);
    in.clear();
    in.seekg(0, std::ios::beg);
//    in.read(static_cast<char*>(&blks_scores_glb[0]),blks_scores_glb_count);
    in.read((char*)&blks_scores_glb[0],blks_scores_glb_count*sizeof(score_t));
  }

  // broadcast all blocks score
  MPI_Bcast(&blks_scores_glb_count, sizeof(blks_scores_glb_count), MPI_BYTE,
    0,MPI_COMM_WORLD);
  LOG_WORKING_ON_DEBUG("Broadcasting " << blks_scores_glb_count  
    << " blocks from rank 0 ...");
  if(mpiRank != 0) {
    blks_scores_glb.resize(blks_scores_glb_count);
  }
  MPI_Bcast(&blks_scores_glb[0],blks_scores_glb_count*sizeof(score_t),MPI_BYTE,0,MPI_COMM_WORLD);
  
  LOG_WORKING_ON_DEBUG(blks_scores_glb_count << " sorted blocks received from rank 0 : ("
    << blks_scores_glb[0].id << "," << blks_scores_glb[0].score << ") -> ("
    << blks_scores_glb[blks_scores_glb_count-1].id << "," 
    << blks_scores_glb[blks_scores_glb_count-1].score << ")");

  // get the list of all blocks to be reduced globally
  std::vector<score_t> to_reduce_glb;
  for(int i=0 ; i < blks_scores_glb_count*Filter::percentage ; i++){
    to_reduce_glb.push_back(blks_scores_glb[i]);
  }

  LOG_WORKING_ON_DEBUG(to_reduce_glb.size()
    << " Total number of blocks to be reduced gloabally");

  // get which blocks to be reduced locally if any
  for(auto &blk_score : blks_scores){
    if(std::count(to_reduce_glb.begin(), to_reduce_glb.end(), blk_score)){
      to_reduce.push_back(blk_score);
    }
  }

  LOG_WORKING_ON_DEBUG (to_reduce.size() 
    << " blocks registred to be reduced locally");

  return to_reduce;
}

int reduce_blocks(std::vector<score_t>& to_reduce, Grid& grid, Attributes& attributes, bool use_mask)
{
  for (const auto &blk_score : to_reduce){
    LOG_WORKING_ON_DEBUG("Reducing block " << blk_score.id << " ...");
    Block* blk = grid.GetBlock(blk_score.id);

    // reduce coordinates
    float* xCoordNew = new float[2];
    float* yCoordNew = new float[2];
    float* zCoordNew = new float[2];
    
    float* xCoordOld = blk->GetXCoord();
    float* yCoordOld = blk->GetYCoord();
    float* zCoordOld = blk->GetZCoord();
    
    xCoordNew[0] = xCoordOld[0];
    yCoordNew[0] = yCoordOld[0];
    zCoordNew[0] = zCoordOld[0];
    
    xCoordNew[1] = xCoordOld[blk->GetXDimSize()-1];
    yCoordNew[1] = yCoordOld[blk->GetYDimSize()-1];
    zCoordNew[1] = zCoordOld[blk->GetZDimSize()-1];
    
    int xDimSizeOld = blk->GetXDimSize();
    int yDimSizeOld = blk->GetYDimSize();
    int zDimSizeOld = blk->GetZDimSize();
    
    blk->SetXDimSize(2);
    blk->SetYDimSize(2);
    blk->SetZDimSize(2);

    blk->SetXCoord(xCoordNew);
    blk->SetYCoord(yCoordNew);
    blk->SetZCoord(zCoordNew);

    // reduce data
    float* newData = new float[2*2*2];
    float* oldData = blk->GetDbzArray();

    if(use_mask){
      std::memset(static_cast<void*>(newData), 0, 8*sizeof(float));
    } else {
      newData[0] = oldData[0];
      newData[1] = oldData[zDimSizeOld-1];
      newData[2] = oldData[zDimSizeOld*(xDimSizeOld-1)];
      newData[3] = oldData[zDimSizeOld*xDimSizeOld-1];
      newData[4] = oldData[zDimSizeOld*xDimSizeOld*(yDimSizeOld-1)];
      newData[5] = oldData[zDimSizeOld*xDimSizeOld*(yDimSizeOld-1)+zDimSizeOld-1];
      newData[6] = oldData[zDimSizeOld*xDimSizeOld*(yDimSizeOld-1)+zDimSizeOld*(xDimSizeOld-1)];
      newData[7] = oldData[zDimSizeOld*xDimSizeOld*yDimSizeOld-1];
    }
    blk->UpdateDbz(newData);
  }
  return 0;
}

