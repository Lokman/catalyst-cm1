#ifndef CM1PRODUCER_HPP
#define CM1PRODUCER_HPP

#include <string>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <dirent.h>
#include <mpi.h>
//#include <hdf5.h>
//#include <H5LTpublic.h>
#include <unistd.h>

#include "CM1DataStructures.hpp"

//namespace CM1Reader
//{

class CM1Producer 
{
public: 
	// TOFIX dsetPath should not be passed to a CM1Producer
	virtual int Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes) = 0;
	virtual int NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes) = 0;
};
//}
#endif

