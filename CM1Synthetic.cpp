#include <iostream>

#include "CM1Synthetic.hpp"
#include "mpi_mclog_m4.hpp"

#include <cstdlib>
#include <ctime>

CM1Synthetic::CM1Synthetic()
{
  LOG_WORKING_ON_DEBUG( "New CM1 synthetic created with config: " 
      << " block extents [" << SYN_NUM_PTS_BLOCK_X << "x"
      << SYN_NUM_PTS_BLOCK_Y << "x" << SYN_NUM_PTS_BLOCK_Z << "] - "
      << "number of blocks per process per dimension ["
      << BlocksNumberOverDomainX << "x" << BlocksNumberOverDomainY << "x"
      << BlocksNumberOverDomainZ << "] - "
      << "number of processes per grid dimension [" << ProcsNumberOverX << "x"
      << ProcsNumberOverY << "x" << ProcsNumberOverZ << "]");
}

int CM1Synthetic::Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes)
{
//  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
  TIMER_SET("CM1SynthInit");

	xDimProcsN = ProcsNumberOverX;
	yDimProcsN = ProcsNumberOverY;
	zDimProcsN = ProcsNumberOverZ;
	xDimBlocksN = BlocksNumberOverDomainX;
	yDimBlocksN = BlocksNumberOverDomainY;
	zDimBlocksN = BlocksNumberOverDomainZ;
	blockExtents[0] = SYN_NUM_PTS_BLOCK_X;
	blockExtents[1] = SYN_NUM_PTS_BLOCK_Y;
	blockExtents[2] = SYN_NUM_PTS_BLOCK_Z;
	wholeExtents[0] = blockExtents[0]*xDimBlocksN*xDimProcsN;
	wholeExtents[1] = blockExtents[1]*yDimBlocksN*yDimProcsN;
	wholeExtents[2] = blockExtents[2]*zDimBlocksN*zDimProcsN;
//	wholeExtents[0] = NUM_PTS_GLOBAL_X;
//	wholeExtents[1] = NUM_PTS_GLOBAL_Y;
//	wholeExtents[2] = NUM_PTS_GLOBAL_Z;

	// intialize Grid and Attributes
	int mpiSize = 1;
	int mpiRank = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

	if ((mpiSize%(xDimProcsN*yDimProcsN*zDimProcsN)) != 0){
		LOG_WORKING_ON_ERROR("The number of processes (" << mpiSize 
			<< ") must be a multiple (equal to?) of " << xDimProcsN*yDimProcsN*zDimProcsN
      << "("<< xDimProcsN << "x" << yDimProcsN 
      << "x" << zDimProcsN << ")");
		MPI_Abort(MPI_COMM_WORLD, 911);
	}

	// TOFIX how to deal with dynamic number of blocks
	unsigned int numPoints[3];
	numPoints[0] = (unsigned int) wholeExtents[0];
	numPoints[1] = (unsigned int) wholeExtents[1];
	numPoints[2] = (unsigned int) wholeExtents[2];
	
  // TOFIX
	//grid.Initialize(numPoints, spacing, numLocalPoints);
	//grid.Initialize(numPoints, numLocalPoints);
	grid.Initialize(numPoints);
	attributes.Initialize(&grid);
	
	this->numBlocks = xDimBlocksN*yDimBlocksN*zDimBlocksN;
	int blockRank = 0;
	for (int blk=0 ; blk<numBlocks ; blk++) {

//		bid_tmp = grid.CreateBlock(blockExtents);
		bid_tmp = grid.CreateBlock();
		blockRank = mpiRank*numBlocks+blk;
		// TOFIX use getters instead?
		float* xCoordBlock = new float[SYN_NUM_PTS_BLOCK_X];// TOFIX Check failure
		float* yCoordBlock = new float[SYN_NUM_PTS_BLOCK_Y];// TOFIX Check failure
		float* zCoordBlock = new float[SYN_NUM_PTS_BLOCK_Z];// TOFIX Check failure
		
		// Set up local coordinates
		for (int i=0 ; i<SYN_NUM_PTS_BLOCK_X ; i++ )
			xCoordBlock[i] = (blockRank%
										(xDimProcsN*xDimBlocksN))*
									SYN_NUM_PTS_BLOCK_X +i;
		for (int i=0 ; i<SYN_NUM_PTS_BLOCK_Y ; i++ )
			yCoordBlock[i] = ((blockRank/
											(xDimProcsN*xDimBlocksN))
										%(yDimProcsN*yDimBlocksN))*
									SYN_NUM_PTS_BLOCK_Y +i;
		for (int i=0 ; i<SYN_NUM_PTS_BLOCK_Z ; i++ )
			zCoordBlock[i] = ((blockRank/
											(xDimProcsN*xDimBlocksN)/
											(yDimProcsN*yDimBlocksN))
										%(zDimProcsN*zDimBlocksN))*
									SYN_NUM_PTS_BLOCK_Z +i;

		grid.SetXLocalCoord(xCoordBlock, bid_tmp);
		grid.SetYLocalCoord(yCoordBlock, bid_tmp);
		grid.SetZLocalCoord(zCoordBlock, bid_tmp);
		
		// TOFIX redundent with extents?
		// TOFIX use dbg_BlockX instead?
		grid.SetXDimSize(SYN_NUM_PTS_BLOCK_X, bid_tmp);
		grid.SetYDimSize(SYN_NUM_PTS_BLOCK_Y, bid_tmp);
		grid.SetZDimSize(SYN_NUM_PTS_BLOCK_Z, bid_tmp);
        
    LOG_WORKING_ON_DEBUG("New block " << blk << "(" << bid_tmp 
      <<") created with coordinates "
      <<"x[" << xCoordBlock[0] << " ... " 
      << xCoordBlock[SYN_NUM_PTS_BLOCK_X-1] << "] - "
      <<"y[" << yCoordBlock[0] << " ... "
      << yCoordBlock[SYN_NUM_PTS_BLOCK_Y-1] << "] - "
      <<"z[" << zCoordBlock[0] << " ... "
      << zCoordBlock[SYN_NUM_PTS_BLOCK_Z-1] << "]");
	}

	numIterations = dbg_NUMITERS;
	currentIteration = 0;

  double res;
  TIMER_GET_MINE("CM1SynthInit", &res, "init");
  LOG_TIMER_INFO(res 
    <<" : CM1Synthetic::Initialize() : CM1Synthetic initialization time");
  
  srand (static_cast <unsigned> (time(0)));
  
  LOG_WORKING_ON_INFO("CM1Synthetic Initialized: " 
        << " Whole Grid initialized and " << grid.GetNumberOfLocalBlocks() 
        << " blocks have been created");
}

int CM1Synthetic::NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes)
{
//	// Timer
//  double synthNextIterLocalS = MPI_Wtime();
//  double synthNextIterLocalF = 0;
  TIMER_SET("CM1SynthIter");

  // TOFIX dont need the -1? do range check
	if (currentIteration == dbg_NUMITERS)
		return 1;
	
  LOG_WORKING_ON_INFO("CM1Synthetic loading data for iteration " 
    << currentIteration  << " ...");

	int timeStep = currentIteration;
	int mpiRank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

  // TOFIX remove blockRank
	int blockRank = 0;
	for (int blk=0 ; blk<this->numBlocks ; blk++) {
		bid_tmp = grid.GetBlockAt(blk)->GetId();
    LOG_WORKING_ON_INFO("Updating block " << blk << "(" << bid_tmp << ") ...");

		blockRank = mpiRank*numBlocks+blk;
		// TOFIX HUGE memory leak!!! Need to free previous arrays
    float* dbz = new float[SYN_NUM_PTS_BLOCK_X*SYN_NUM_PTS_BLOCK_Y*SYN_NUM_PTS_BLOCK_Z];	
		// TOFIX Dont remember why I chose the values
    float min = 4;
    float max = 70;
		for (int i=0 ; i<(SYN_NUM_PTS_BLOCK_X*SYN_NUM_PTS_BLOCK_Y*SYN_NUM_PTS_BLOCK_Z) ; i++) 
      dbz[i] = min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
//      dbz[i] = mpiRank*6547+ i*415 + 43*timeStep;
		
//		attributes.UpdateDbz(dbz, bid_tmp);
		attributes.UpdateUinterp(dbz, bid_tmp);
//		attributes.UpdateVinterp(dbz, bid_tmp);
		attributes.SetTimeStep(timeStep);
		
    LOG_CM1SYNTH_DEBUG("Data @" << dbz << "-" 
      << attributes.GetDbzArray(bid_tmp) 
      << "of Block " << blk << " (" << bid_tmp 
      << ") Iteration " << attributes.GetCurrentTimeStep() 
      << " made available by CM1Synthetic");
	}
  
//  // Timer
//  synthNextIterLocalF = MPI_Wtime();
  double res;
  TIMER_GET_MINE("CM1SynthIter", &res, attributes.GetCurrentTimeStep());
  LOG_TIMER_INFO(res << " : CM1Synth::NextIteration() : TimeStep " 
    << attributes.GetCurrentTimeStep());

	currentIteration++;
	return 0;
}

