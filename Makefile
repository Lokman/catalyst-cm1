# Basic Makefile with vtk dependencies libraries
#CC = mpic++ -g3 -Wall -std=c++11
# TODO check if I can add directly to CC command, LDFLAGS may cause pbs
#CC = mpic++ -g3 -std=c++11 $(CPPFLAGS) $(LDFLAGS)
CPPFLAGS_ = $$CPPFLAGS 
CC = mpic++ -g3 -std=c++11 $(CPPFLAGS_)
# TODO check if it is the right env vars
LDFLAGS_ = $$LDFLAGS
LDFLAGS_CATALYST = -L/opt/lib/paraview-5.0/ \
 -lvtkUtilitiesPythonInitializer-pv5.0 -lvtkPVCatalyst-pv5.0 \
 -lvtkCommonDataModel-pv5.0 -lvtkCommonCore-pv5.0 -lvtkPVPythonCatalyst-pv5.0 \
 -lvtkParallelCore-pv5.0
#all:	mclog-m4 cm1catalyst

# TOFIX still need to export those vars before running
export LD_LIBRARY_PATH=/opt/lib/paraview-5.0/:$$LD_LIBRARY_PATH
export PYTHONPATH=$$PYTHONPATH:/opt/lib/paraview-5.0/site-packages/
export PYTHONPATH=$$PYTHONPATH:/opt/lib/paraview-5.0/
export PYTHONPATH=$$PYTHONPATH:/opt/lib/paraview-5.0/site-packages/vtk


mpi_mclog_m4.hpp: mpi_mclog_m4.hpp.m4 channels_defs.hpp 
		m4 mpi_mclog_m4.hpp.m4 > mpi_mclog_m4.hpp
mpi_mclog_m4.o: mpi_mclog_m4.hpp  mpi_mclog_m4.cpp
	$(CC) -o mpi_mclog_m4.o -c mpi_mclog_m4.cpp

# TOFIX
CM1Filter.o: CM1Filter.hpp CM1Filter.cpp
	$(CC) -o CM1Filter.o -c CM1Filter.cpp

#cm1catalyst-bil:
#	$(CC) mpi_mclog_m4.cpp CM1DataStructures.cpp CM1Adaptor.cpp CM1ReaderWk100.cpp CM1Synthetic.cpp CM1Driver.cpp CM1BWC16Dbz.cpp -I/opt/include/paraview-5.0/ -L/opt/lib/paraview-5.0/ -I/usr/include/hdf5/serial/ -L/usr/lib/x86_64-linux-gnu/hdf5/serial/ -I/opt/include -L/opt/lib/ -lbil -lhdf5_hl -lhdf5 -lvtkUtilitiesPythonInitializer-pv5.0 -lvtkPVCatalyst-pv5.0 -lvtkCommonDataModel-pv5.0 -lvtkCommonCore-pv5.0 -lvtkPVPythonCatalyst-pv5.0 -lvtkParallelCore-pv5.0
#
#cm1catalyst-ceph:
#	$(CC) mpi_mclog_m4.cpp CM1DataStructures.cpp CM1Adaptor.cpp CM1ReaderWk100.cpp CM1Synthetic.cpp CM1Driver.cpp CM1BWC16Dbz.cpp -I/opt/include/paraview-5.0/ -L/opt/lib/paraview-5.0/ -I/usr/include/hdf5/serial/ -L/usr/lib/x86_64-linux-gnu/hdf5/serial/ -lrados -lhdf5_hl -lhdf5 -lvtkUtilitiesPythonInitializer-pv5.0 -lvtkPVCatalyst-pv5.0 -lvtkCommonDataModel-pv5.0 -lvtkCommonCore-pv5.0 -lvtkPVPythonCatalyst-pv5.0 -lvtkParallelCore-pv5.0
#
#cm1catalyst:
#	$(CC) mpi_mclog_m4.cpp CM1DataStructures.cpp CM1Adaptor.cpp CM1ReaderWk100.cpp CM1Synthetic.cpp CM1Driver.cpp CM1BWC16Dbz.cpp -I/opt/include/paraview-5.0/ -L/opt/lib/paraview-5.0/ -I/usr/include/hdf5/serial/ -L/usr/lib/x86_64-linux-gnu/hdf5/serial/ -lhdf5_hl -lhdf5 -lvtkUtilitiesPythonInitializer-pv5.0 -lvtkPVCatalyst-pv5.0 -lvtkCommonDataModel-pv5.0 -lvtkCommonCore-pv5.0 -lvtkPVPythonCatalyst-pv5.0 -lvtkParallelCore-pv5.0

 
cm1synth-catalyst: LDFLAGS_ += $(LDFLAGS_CATALYST)
cm1synth-catalyst: mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o CM1Synthetic.o \
                   CM1Driver-Synth.o
cm1synth-catalyst: 
	$(CC) -o cm1synth-catalyst mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
	CM1Synthetic.o CM1Driver-Synth.o $(LDFLAGS_)

cm1bwc16dbz-mpiio-catalyst: LDFLAGS_ += $(LDFLAGS_CATALYST)
cm1bwc16dbz-mpiio-catalyst: mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
                            CM1BWC16Dbz-MPIIO.o CM1Driver-BWC16Dbz.o
cm1bwc16dbz-mpiio-catalyst: 
	$(CC) -o cm1bwc16dbz-mpiio-catalyst mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
	CM1BWC16Dbz-MPIIO.o CM1Driver-BWC16Dbz.o $(LDFLAGS_)

cm1bwc16dbz-bil-catalyst: LDFLAGS_ += $(LDFLAGS_CATALYST) -L/opt/lib/ -lbil
cm1bwc16dbz-bil-catalyst: mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
                            CM1BWC16Dbz-BIL.o CM1Driver-BWC16Dbz-BIL.o
cm1bwc16dbz-bil-catalyst: 
	$(CC) -o cm1bwc16dbz-bil-catalyst mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
	CM1BWC16Dbz-BIL.o CM1Driver-BWC16Dbz.o $(LDFLAGS_)

# TOFIX manualy adding CM1Filter object files
cm1bwc16dbz-ceph-catalyst: LDFLAGS_ += $(LDFLAGS_CATALYST) -lrados
cm1bwc16dbz-ceph-catalyst: mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
                            CM1BWC16Dbz-Ceph.o CM1Driver-BWC16Dbz.o CM1Filter.o
cm1bwc16dbz-ceph-catalyst: 
	$(CC) -o cm1bwc16dbz-ceph-catalyst mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
	CM1BWC16Dbz-Ceph.o CM1Driver-BWC16Dbz.o CM1Filter.o $(LDFLAGS_)

# TODO get rid of Catalyst & adaptor
cm1bwc16dbz-ceph-store: LDFLAGS_ += $(LDFLAGS_CATALYST) -lrados
cm1bwc16dbz-ceph-store: mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o\
                            CM1BWC16Dbz-CephStore.o CM1Driver-BWC16Dbz.o
cm1bwc16dbz-ceph-store: 
	$(CC) -o cm1bwc16dbz-ceph-store mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
	CM1BWC16Dbz-CephStore.o CM1Driver-BWC16Dbz.o $(LDFLAGS_)

cm1wk100-catalyst: LDFLAGS_ += $(LDFLAGS_CATALYST) \
                   -L/usr/lib/x86_64-linux-gnu/hdf5/serial/ -lhdf5_hl -lhdf5
cm1wk100-catalyst: mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o CM1ReaderWk100.o \
                   CM1Driver-Wk100.o
cm1wk100-catalyst: 
	$(CC) -o cm1wk100-catalyst mpi_mclog_m4.o CM1DataStructures.o CM1Adaptor.o \
	CM1ReaderWk100.o CM1Driver-Wk100.o $(LDFLAGS_)

CM1DataStructures.o : CM1DataStructures.cpp CM1DataStructures.hpp \
                      mpi_mclog_m4.o
	$(CC) -c CM1DataStructures.cpp -o CM1DataStructures.o 

CM1Adaptor.o : CPPFLAGS_ += -I/opt/include/paraview-5.0/ 
## TOFIX chain multiple variable setting
#CM1Adaptor.o : LDFLAGS_ += -L/opt/lib/paraview-5.0/ \
#							 -lvtkUtilitiesPythonInitializer-pv5.0 -lvtkPVCatalyst-pv5.0 \
#							 -lvtkCommonDataModel-pv5.0 -lvtkCommonCore-pv5.0 \
#							 -lvtkPVPythonCatalyst-pv5.0 -lvtkParallelCore-pv5.0 
CM1Adaptor.o : CM1Adaptor.cpp CM1Adaptor.hpp \
               mpi_mclog_m4.o
	$(CC) -c CM1Adaptor.cpp $(LDFLAGS_) -o CM1Adaptor.o 
#	$(CC) -c CM1Adaptor.cpp $(LDFLAGS_) -o CM1Adaptor.o 

# TODO check if comma is allowed - nope it isnt
CM1ReaderWk100.o : CPPFLAGS_ +=  -I/usr/include/hdf5/serial/
CM1ReaderWk100.o : CM1ReaderWk100.cpp CM1ReaderWk100.hpp \
                   CM1Producer.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1ReaderWk100.cpp  $(LDFLAGS_) -o CM1ReaderWk100.o 

CM1Synthetic.o : CM1Synthetic.cpp CM1Synthetic.hpp \
                 CM1Producer.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1Synthetic.cpp -o CM1Synthetic.o 

CM1BWC16Dbz-MPIIO.o : CPPFLAGS_ += -DUSE_MPI_IO 
CM1BWC16Dbz-MPIIO.o : CM1BWC16Dbz.cpp CM1BWC16Dbz.hpp \
                CM1Producer.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1BWC16Dbz.cpp -o CM1BWC16Dbz-MPIIO.o 

CM1BWC16Dbz-BIL.o : CPPFLAGS_ += -DUSE_BIL -I/opt/include
CM1BWC16Dbz-BIL.o : CM1BWC16Dbz.cpp CM1BWC16Dbz.hpp \
                CM1Producer.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1BWC16Dbz.cpp -o CM1BWC16Dbz-BIL.o 

CM1BWC16Dbz-Ceph.o : CPPFLAGS_ += -DUSE_CEPH_OBJECTS  
CM1BWC16Dbz-Ceph.o : CM1BWC16Dbz.cpp CM1BWC16Dbz.hpp \
                CM1Producer.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1BWC16Dbz.cpp -o CM1BWC16Dbz-Ceph.o 

CM1BWC16Dbz-CephStore.o : CPPFLAGS_ += -DUSE_CEPH_STORE
CM1BWC16Dbz-CephStore.o : CM1BWC16Dbz.cpp CM1BWC16Dbz.hpp \
                CM1Producer.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1BWC16Dbz.cpp -o CM1BWC16Dbz-CephStore.o 

# TODO make CM1Adaptor & Catalyst optional
CM1Driver-Synth.o: CPPFLAGS_ += -DPROD_SYNTH
CM1Driver-Synth.o: CM1Driver.cpp \
             CM1Synthetic.hpp CM1Adaptor.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1Driver.cpp -o CM1Driver-Synth.o

CM1Driver-Wk100.o: CPPFLAGS_ += -DPROD_WK100 -I/usr/include/hdf5/serial/ 
CM1Driver-Wk100.o: CM1Driver.cpp \
             CM1ReaderWk100.hpp CM1Adaptor.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1Driver.cpp -o CM1Driver-Wk100.o

CM1Driver-BWC16Dbz.o: CPPFLAGS_ += -DPROD_BWC16DBZ
CM1Driver-BWC16Dbz.o: CM1Driver.cpp \
             CM1BWC16Dbz.hpp CM1Adaptor.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1Driver.cpp -o CM1Driver-BWC16Dbz.o

CM1Driver-BWC16Dbz-BIL.o: CPPFLAGS_ += -DPROD_BWC16DBZ -DUSE_BIL -I/opt/include
CM1Driver-BWC16Dbz-BIL.o: CM1Driver.cpp \
             CM1BWC16Dbz.hpp CM1Adaptor.hpp CM1DataStructures.hpp mpi_mclog_m4.o
	$(CC) -c CM1Driver.cpp -o CM1Driver-BWC16Dbz.o
clear: 
	rm *.o
