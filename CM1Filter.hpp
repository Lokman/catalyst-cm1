/* These set of functions allows to reduce data values of blocks 
 * with regards to a global percentage:
 *  1- compute a data variability score for each block
 *  2- do a collective blocks sort by their scores
 *  3- compute the blocks to be reduced that cover the specified percentage
 *  4- reduce blocks localy
 */

#ifndef CM1_FILTER_HPP
#define CM1_FILTER_HPP

#include "CM1DataStructures.hpp"

typedef struct score_t {
  int id;
  float score;
  score_t () {}
  score_t (int bid, float bscore): id(bid), score(bscore) {}
  bool operator<(const score_t& other) const {
    if (score != other.score)
      return score < other.score;
    else 
      return id < other.id;
  }

  inline bool operator==(const score_t& other) {
    return (id == other.id) && (score == other.score);
  }
} score_t;

namespace Filter
{

int Initialize(float percentage);
int Filter(Grid& grid, Attributes& attributes);
int Finalize();

extern float percentage;
//extern std::vector<score_t> blks_scores;
extern std::string basename;

}

#endif

