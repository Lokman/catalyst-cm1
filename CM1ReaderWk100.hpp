/*
 * Based on Matthieu Dorier's source code 
 * AND ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#ifndef CM1READER_HPP
#define CM1READER_HPP

#include <string>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <dirent.h>
#include <mpi.h>
#include <hdf5.h>
#include <H5LTpublic.h>
#include <unistd.h>

#include "CM1Producer.hpp"
#include "CM1DataStructures.hpp"
#include "mpi_mclog_m4.hpp"

//namespace CM1Reader
//{

class CM1ReaderWk100 : public CM1Producer
{
public:
	int Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes);
	int NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
	// TOFIX lets keep these members public for know ...
	// TOFIX how to intergrate that with less effort in existing simulations
	void list_iterations(const char*);
	int read_metadata(const std::string& dsetPath);
	int read_datasets(const char*,int,int,float**);
	void do_iteration(MPI_Comm,int);

private:
	float *dbzVar,*uinterpVar, *vinterpVar;
	float *xCoord, *yCoord, *zCoord;
	int xDimSize, yDimSize, zDimSize;
	int * iterations;
	int numIterations;
	int currentIteration;
	std::string dataSet; // TOFIX is it needed
	int bid_tmp;
};

//}
#endif

