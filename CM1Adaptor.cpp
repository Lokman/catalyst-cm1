/*
 * Based on ParaViewCatalyst Example 
 * https://github.com/Kitware/ParaViewCatalystExampleCode/tree/master/CxxMultiPieceExample
*/

#include <iostream>
#include <mpi.h>

#include "CM1Adaptor.hpp"

#include <vtkCellData.h>
#include <vtkCellType.h>
#include <vtkCPDataDescription.h>
#include <vtkCPInputDataDescription.h>
#include <vtkCPProcessor.h>
#include <vtkCPPythonScriptPipeline.h>
#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkNew.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkImageData.h>
#include <vtkRectilinearGrid.h>
#include <vtkMultiBlockDataSet.h>
#include <vtkMultiPieceDataSet.h>

vtkCPProcessor* Processor = NULL;
vtkMultiBlockDataSet* VTKGrid = NULL;

#define USE_MULTI_BLOCK_DATASET
//#define USE_MULTI_PIECE_DATASET

namespace Adaptor
{

void BuildVTKGrid(Grid& grid)
{
	int mpiSize = 1;
	int mpiRank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);


#ifdef USE_MULTI_BLOCK_DATASET
		VTKGrid->SetNumberOfBlocks(mpiSize*grid.GetNumberOfLocalBlocks());
#endif

  // TOFIX make it more optimal
	int numBlocks = grid.GetNumberOfLocalBlocks();
	for ( int blk=0 ; blk < numBlocks ; blk++){
		int bid = grid.GetBlockAt(blk)->GetId();
		int blkNbr = blk+mpiRank*numBlocks; 
    LOG_CM1ADAPTOR_DEBUG("Adding block " << blk 
			<< "(" << bid << ")" <<" of " << numBlocks 
			<< " coord [@" << grid.GetXLocalCoord(bid) 
			<< ",@" << grid.GetYLocalCoord(bid) 
			<< ",@" << grid.GetZLocalCoord(bid) 
			<< "] of size [" << grid.GetXDimSize(bid) << ","
			<< grid.GetYDimSize(bid) << ","
			<< grid.GetZDimSize(bid)
			<< "]  to vtk Grid");
		
		vtkNew<vtkFloatArray> xArray, yArray, zArray;
		xArray->SetArray(grid.GetXLocalCoord(bid), static_cast<vtkIdType>(grid.GetXDimSize(bid)), 1);
		yArray->SetArray(grid.GetYLocalCoord(bid), static_cast<vtkIdType>(grid.GetYDimSize(bid)), 1);
		zArray->SetArray(grid.GetZLocalCoord(bid), static_cast<vtkIdType>(grid.GetZDimSize(bid)), 1);
	
		vtkNew<vtkRectilinearGrid> rectGrid;
		// TOFIX Is this call needed?
		// TOFIX or is it supposed to be for the whole extent?
		rectGrid->SetDimensions(grid.GetXDimSize(bid),grid.GetYDimSize(bid), grid.GetZDimSize(bid));
		rectGrid->SetXCoordinates(xArray.GetPointer());
		rectGrid->SetYCoordinates(yArray.GetPointer());
		rectGrid->SetZCoordinates(zArray.GetPointer());

//    //rectGrid->AllocateCellGhostArray();
//    //rectGrid->UpdateCellGhostArrayCache();
//    int ext[3] = {grid.GetGlobalExtents()[1] - grid.GetGlobalExtents()[0] + 1,
//        grid.GetGlobalExtents()[3] - grid.GetGlobalExtents()[2] + 1,
//        grid.GetGlobalExtents()[5] - grid.GetGlobalExtents()[4] + 1,
//        };
//    rectGrid->GenerateGhostArray(ext);
	
#ifdef USE_MULTI_PIECE_DATASET
		// TOFIX not up to date with multi-blocks
		// TOFIX add multiblock support to MultiPieceDataSet
		// using vtkMultiPiece
		vtkNew<vtkMultiPieceDataSet> multiPiece;
		multiPiece->SetNumberOfPieces(mpiSize);
		multiPiece->SetPiece(mpiRank, rectGrid.GetPointer());
		VTKGrid->SetNumberOfBlocks(1);
		VTKGrid->SetBlock(0, multiPiece.GetPointer());
#else
		// using vtkMultiBlock only
		// Using a C-like array indexing as indentifiers
		// TOFIX assumes each process has the same number of blocks
		VTKGrid->SetNumberOfBlocks(mpiSize*grid.GetNumberOfLocalBlocks());
		VTKGrid->SetBlock(blkNbr,rectGrid.GetPointer());
#endif
		LOG_CM1ADAPTOR_DEBUG("Block " << blk << "(" << bid << ")" << "/" << numBlocks 
			<< " added to vtk Grid as block number " << blkNbr);	
	} // Loop over blocks
}

void UpdateVTKAttributes(Grid& grid, Attributes& attributes)
{
	int mpiSize = 1;
	int mpiRank = 0;
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

	// TOFIX make it more optimal
	int numBlocks = grid.GetNumberOfLocalBlocks();
	for ( int blk=0 ; blk < numBlocks ; blk++){
	
		int bid = grid.GetBlockAt(blk)->GetId();
		int blkNbr = blk+mpiRank*numBlocks; 
		LOG_CM1ADAPTOR_DEBUG("Adding block " << blk << "(" << bid << ")" << " of " << numBlocks << " to vtk Attributes");

#ifdef USE_MULTI_PIECE_DATASET
		// TOFIX not up to date with multi-blocks
		// TOFIX add multiBlocks support to MultiPieceDataSet
		// using vtkMultiPiece
		vtkMultiPieceDataSet* multiPiece = vtkMultiPieceDataSet::SafeDownCast(VTKGrid->GetBlock(0));
		vtkDataSet* dataSet = vtkDataSet::SafeDownCast(multiPiece->GetPiece(blk+mpiRank*numBlock));
#else
		// using vtkMultiBlock only
		vtkDataSet* dataSet = vtkDataSet::SafeDownCast(VTKGrid->GetBlock(blkNbr));

#endif

		if(dataSet->GetPointData()->GetNumberOfArrays() == 0)
		{
			LOG_CM1ADAPTOR_DEBUG("Adding uinterp data array of block " << blk << "/" << numBlocks << " to vtk Attributes for the first time");
			vtkNew<vtkFloatArray> array;
			array->SetName("uinterp");
			array->SetNumberOfComponents(1);
			dataSet->GetPointData()->AddArray(array.GetPointer());
		}

		vtkFloatArray* uinterp = vtkFloatArray::SafeDownCast(dataSet->GetPointData()->GetArray("uinterp"));
		LOG_CM1ADAPTOR_DEBUG("Adding uinterp data array @" << attributes.GetUinterpArray(bid) << " of size " << grid.GetNumberOfPoints(bid) << " to vtk Attributes");
		uinterp->SetArray(attributes.GetUinterpArray(bid), static_cast<vtkIdType>(grid.GetNumberOfPoints(bid)), 1);

	} // Loop over blocks
	
}

// TODO Change the blocks parameter to the Grid class
void BuildVTKDataStructures(Grid& grid, Attributes& attributes)
{
	if (VTKGrid == NULL){	
		LOG_CM1ADAPTOR_DEBUG("Building VTKGrid for the first time");
		VTKGrid = vtkMultiBlockDataSet::New();
		BuildVTKGrid(grid);
	}
	UpdateVTKAttributes(grid, attributes);
}


int Initialize (const char* scriptPath)
{
//  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
////  LOG_SET_LEVEL_WORKING_ON(LOG_NONE);
////  LOG_SET_LEVEL_CM1ADAPTOR(LOG_NONE);
//  LOG_SET_COLOR_TIMER(LOG_GREEN_BKG);

	if(Processor == NULL)
	{
		Processor = vtkCPProcessor::New();
		Processor->Initialize();
	} else 
	{
		Processor->RemoveAllPipelines();
	}
	
	vtkNew<vtkCPPythonScriptPipeline> pipeline;
	if (pipeline->Initialize(scriptPath) == 0){
		std::cerr << "Cannot read python script " << scriptPath << std::endl;
		return 1;
	}

	Processor->AddPipeline(pipeline.GetPointer());
	return 0;
}

int Finalize()
{
	if(Processor)
	{
		Processor->Delete();
		Processor = NULL;
	}
	if(VTKGrid)
	{
		VTKGrid->Delete();
		VTKGrid = NULL;
	}

	return 0;
}

int CoProcess(Grid& grid, Attributes& attributes)
{
	int timestep = attributes.GetCurrentTimeStep();
	vtkNew<vtkCPDataDescription> dataDescription;
	dataDescription->AddInput("input");
	dataDescription->SetTimeData(timestep,timestep);
	// DEBUG
	//dataDescription->PrintSelf(std::cout, (vtkIndent)0);
	
  // Timer
//  double coProcessLocalS = 0;
//  double coProcessLocalF = 0;
  double res;

  dataDescription->ForceOutputOn();
	if(Processor->RequestDataDescription(dataDescription.GetPointer()) != 0)
	{
		
//    // Timer 
//    coProcessLocalS = MPI_Wtime();
    TIMER_SET("BuildVTKDSs");    
    BuildVTKDataStructures(grid, attributes);
//    // Timer
//    coProcessLocalF = MPI_Wtime();
    TIMER_GET_MINE("BuildVTKDSs", &res, timestep);
    LOG_TIMER_DEBUG(res 
      << " : Adaptor::CoProcess::BuildVTKGrid() : Building vtk for TimeStep " << timestep);
		
    dataDescription->GetInputDescriptionByName("input")->SetGrid(VTKGrid);
		LOG_CM1ADAPTOR_DEBUG("Processing the vtk Grid & attributes of Iteration " << timestep << " by Catalyst");
		
    LOG_CM1ADAPTOR_INFO("vtk Grid Global Extents are @"
        << grid.GetGlobalExtents()
        << "[" << grid.GetGlobalExtents()[1] - grid.GetGlobalExtents()[0] + 1
        << "," << grid.GetGlobalExtents()[3] - grid.GetGlobalExtents()[2] + 1
        << "," << grid.GetGlobalExtents()[5] - grid.GetGlobalExtents()[4] + 1
        << "] -  Rendering ...");

//    // Timer
//    coProcessLocalS = MPI_Wtime();
    TIMER_SET("vtkCCPCoProcess");
    // TOFIX It seems that it have no effect on the viz result
		dataDescription->GetInputDescriptionByName("input")->SetWholeExtent(grid.GetGlobalExtents());
		Processor->CoProcess(dataDescription.GetPointer());
    
//    // Timer
//    coProcessLocalF = MPI_Wtime();
    TIMER_GET_MINE("vtkCCPCoProcess", &res, timestep);
    LOG_TIMER_DEBUG(res
      << " : Adaptor::CoProcess::CoProcess() : Rendering TimeStep " << timestep);
	}

	return 0;
}

}


