#include "DataStructures.hpp"

#include <mpi.h>
#include <iostream>

Grid::Grid()
{}

void Grid::Initialize(const unsigned int numPoints[3], const double spacing[3], const unsigned int numLocalPoints[3])
{
  if(numPoints[0] == 0 || numPoints[1] == 0 || numPoints[2] == 0)
    {
    std::cerr << "Must have a non-zero amount of points in each direction.\n";
    }
  for(int i=0;i<3;i++)
    {
    this->Extents[i*2] = 0;
    this->Extents[i*2+1] = numLocalPoints[i];
    this->GlobalExtents[i*2] = 0;
    this->GlobalExtents[i*2+1] = numPoints[i];
    this->Spacing[i] = spacing[i];
    }

	// Set up local coordinates
	int xOffset = 0;
	int yOffset = 0;
	int zOffset = 0;

	int mpiSize = 1;
	int mpiRank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);

	xOffset = ((mpiRank/3)%16)*6000 - 48000;
	yOffset = ((mpiRank/3)/16)*6000 - 48000;
	zOffset = 0;

	this->xLocalCoord.resize(numLocalPoints[0]);
	this->yLocalCoord.resize(numLocalPoints[1]);
	this->zLocalCoord.resize(numLocalPoints[2]);
	int j;
	for(j=0; j<numLocalPoints[0]; j++) this->xLocalCoord[j] = (float)(xOffset + j*100);
	for(j=0; j<numLocalPoints[1]; j++) this->yLocalCoord[j] = (float)(yOffset + j*100);
	for(j=0; j<numLocalPoints[2]; j++) this->zLocalCoord[j] = (float)(zOffset + j*200);

	
	// in parallel, we do a simple partitioning in the x-direction.
//  int mpiSize = 1;
//  int mpiRank = 0;
//  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
//  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
//
//  this->Extents[0] = mpiRank*numPoints[0]/mpiSize;
//  this->Extents[1] = (mpiRank+1)*numPoints[0]/mpiSize;
}

unsigned int Grid::GetNumberOfLocalPoints()
{
  return (this->Extents[1]-this->Extents[0]+1) *
    (this->Extents[3]-this->Extents[2]+1) *
    (this->Extents[5]-this->Extents[4]+1);
}

unsigned int Grid::GetNumberOfLocalCells()
{
  return (this->Extents[1]-this->Extents[0]) *
    (this->Extents[3]-this->Extents[2]) *
    (this->Extents[5]-this->Extents[4]);
}

//void Grid::GetPoint(unsigned int logicalLocalCoords[3], double coord[3])
//{
//  coord[0] = (this->Extents[0]+logicalLocalCoords[0])*this->Spacing[0];
//  coord[1] = logicalLocalCoords[1]*this->Spacing[1];
//  coord[2] = logicalLocalCoords[2]*this->Spacing[2];
//}

double* Grid::GetSpacing()
{
  return this->Spacing;
}

unsigned int* Grid::GetExtents()
{
  return this->Extents;
}

float* Grid::GetXLocalCoord()
{
	return &(this->xLocalCoord[0]);
}


float* Grid::GetYLocalCoord()
{
	return &(this->yLocalCoord[0]);
}

float* Grid::GetZLocalCoord()
{
	return &(this->zLocalCoord[0]);
}

Attributes::Attributes()
{
  this->GridPtr = NULL;
}

void Attributes::Initialize(Grid* grid)
{
  this->GridPtr = grid;
}

void Attributes::UpdateFields(float* data, double time)
{
//  fprintf(stderr, "----- debug:Attributes:UpdateFields 0\n");
	size_t numCells = this->GridPtr->GetNumberOfLocalCells();
//	size_t numPoints = this->GridPtr->GetNumberOfLocalPoints();
  this->Dbz.resize(numCells);
//  fprintf(stderr, "----- debug:Attributes:UpdateFields 1\n");

	unsigned int* extents = this->GridPtr->GetExtents();
//  unsigned int logicalLocalCoords[3];
  size_t pt = 0;
//  fprintf(stderr, "----- debug:Attributes:UpdateFields 2\n");
//  for(unsigned int k=0;k<extents[5]-extents[4]+1;k++)
  for(unsigned int k=0;k<extents[5]-extents[4];k++)
    {
//    logicalLocalCoords[2] = k;
//    for(unsigned int j=0;j<extents[3]-extents[2]+1;j++)
    for(unsigned int j=0;j<extents[3]-extents[2];j++)
      {
//      logicalLocalCoords[1] = j;
//      for(unsigned int i=0;i<extents[1]-extents[0]+1;i++)
      for(unsigned int i=0;i<extents[1]-extents[0];i++)
        {
//        logicalLocalCoords[0] = i;
//        double coord[3];
//        this->GridPtr->GetPoint(logicalLocalCoords, coord);
//        this->Velocity[pt] = coord[1]*time;
//        pt++;
//  				fprintf(stderr, "----- debug:Attributes:UpdateFields saving the %d value, Dbz vector size %d - data size %d\n", pt, this->Dbz.size(), this->GridPtr->GetNumberOfLocalCells());
					float* dataElementPtr = (float*)data+ i*(extents[3]-extents[2])*(extents[1]-extents[0])+j*(extents[1]-extents[0])+k;
					this->Dbz[pt++] = *dataElementPtr;
        }
      }
    }

//  std::fill(this->Velocity.begin()+numPoints, this->Velocity.end(), 0.);
//  std::fill(this->Pressure.begin(), this->Pressure.end(), 1.);
}

float* Attributes::GetDbzArray()
{
  return &(this->Dbz[0]);
}
