#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <cstddef>
#include <vector>

class Grid
{
public:
	Grid();
	void Initialize(const unsigned int numGlobalPoints[3], const double spacing[3], const unsigned int numLocalPoints[3]);
	unsigned int GetNumberOfLocalPoints();
	unsigned int GetNumberOfLocalCells();
//	void GetPoint(unsigned int logicalLocalCoords[3], double coord[3]);
	double* GetSpacing();
	float* GetXLocalCoord();
	float* GetYLocalCoord();
	float* GetZLocalCoord();

	unsigned int* GetExtents();
private:
	unsigned int Extents[6];
	unsigned int GlobalExtents[6];
	double Spacing[3];
	//std::vector<float> XLocalCoord;
	std::vector<float> xLocalCoord;
	std::vector<float> yLocalCoord;
	std::vector<float> zLocalCoord;
//	float xLocalCoord[60];
//	float yLocalCoord[60];
//	float zLocalCoord[100];
};

class Attributes
{
// A class for generating and storing point and cell fields.
// Velocity is stored at the points and pressure is stored
// for the cells. The current velocity profile is for a
// shearing flow with U(y,t) = y*t, V = 0 and W = 0.
// Pressure is constant through the domain.
public:
	Attributes();
	void Initialize(Grid* grid);
	void UpdateFields(float* data, double time);
	float* GetDbzArray();

private:
	std::vector<float> Dbz;
	Grid* GridPtr;
};
#endif
