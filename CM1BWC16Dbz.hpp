#ifndef CM1BWC16DBZ_DEF
#define CM1BWC16DBZ_DEF

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstring>

#include <algorithm>
#include <dirent.h>

#include "CM1Producer.hpp"
#include "mpi_mclog_m4.hpp"

#ifdef USE_BIL
#include <bil.h>
#endif

#if defined (USE_CEPH_File) || defined (USE_CEPH_OBJECTS) || defined (USE_CEPH_STORE)
#include <rados/librados.h>
#endif


// TOFIX check with coordinates number
#define NUM_PTS_GLOBAL_X 2200//2200
#define NUM_PTS_GLOBAL_Y 2200//2200
#define NUM_PTS_GLOBAL_Z 380//380

//TOFIX variable blocks' size
#define NUM_PTS_BLOCK_X 55//275//55
#define NUM_PTS_BLOCK_Y 55//275//55
#define NUM_PTS_BLOCK_Z 38//190//38

class CM1BWC16Dbz : public CM1Producer
{
public:
  CM1BWC16Dbz(const std::string& dsetPath, const std::string& coordFile);
	int Initialize(const std::string& dsetPath, Grid& grid, Attributes& attributes);
	int NextIteration(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
  int read_global_coordinates();
  int read_dataset();
  int list_iterations();
private:
  std::string dsetPath;
  std::string coordFile;
  std::vector<std::string> iters;
  // TOFIX Don't need all coord for each proc
  std::vector<float> xCoordGlobal;
  std::vector<float> zCoordGlobal;
  std::vector<float> yCoordGlobal;
  std::vector<float> xCoordLocal;
  std::vector<float> zCoordLocal;
  std::vector<float> yCoordLocal;
  
  long numIterations;
  long currentIteration;
  long blocksNbr;

#ifdef USE_MPI_IO
  int NextIterationMPIIO(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
#endif
#ifdef USE_BIL
  int NextIterationBIL(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
#endif
#ifdef USE_CEPH_FILE
  int NextIterationCephFile(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
#endif
#ifdef USE_CEPH_OBJECTS
  int NextIterationCephObjects(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
#endif
#ifdef USE_CEPH_STORE
  int NextIterationCephStore(MPI_Comm mpiComm, Grid& grid, Attributes& attributes);
#endif
};



#endif

